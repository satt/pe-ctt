// File Helpers.h
#ifndef ICT07_2_HELPERS_H
#define ICT07_2_HELPERS_H

#include <iostream>

//#define HAVE_GETTIMEOFDAY

#define HARD_WEIGHT_SET
const int HARD_WEIGHT = 1; // managed by the weights

#include "EasyLocal.hh"
#include "Helpers.hh"
#include "TT_BasicClasses.hh"


/***************************************************************************
 * Cost Components
 ***************************************************************************/

// UnscheduledEvents:
// v1. min number of students in the two event
// v2. number of clashes

class StudentClash : public CostComponent<TT_Input,TT_State>
{
public:
  StudentClash(const TT_Input& in, int w, bool hard) : 
    CostComponent<TT_Input,TT_State>(in,w,hard,"Student Clash")
  {}
  int ComputeCost(const TT_State& st) const;
  void PrintViolations(const TT_State& st, std::ostream& os = std::cout) const;
};

class StudentClash2 : public CostComponent<TT_Input,TT_State>
{
public:
  StudentClash2(const TT_Input& in, int w, bool hard) : 
    CostComponent<TT_Input,TT_State>(in,w,hard,"Student Clash2")
  {}
  int ComputeCost(const TT_State& st) const;
  void PrintViolations(const TT_State& st, std::ostream& os = std::cout) const;
};

class EventPrecedence : public CostComponent<TT_Input,TT_State>
{
public:
  EventPrecedence(const TT_Input& in, int w, bool hard) : 
    CostComponent<TT_Input,TT_State>(in,w,hard,"Event Precedence") {}
  int ComputeCost(const TT_State& st) const;
  void PrintViolations(const TT_State& st, std::ostream& os = std::cout) const;
};

// UnscheduledEvents:
// v1. number of students in the unscheduled event
// v2. number of unscheduled events

class UnscheduledEvents : public CostComponent<TT_Input,TT_State>
{
public:
  UnscheduledEvents(const TT_Input& in, int w, bool hard) : 
    CostComponent<TT_Input,TT_State>(in,w,hard,"UnscheduledEvents")
  {}
  int ComputeCost(const TT_State& st) const;
  void PrintViolations(const TT_State& st, std::ostream& os = std::cout) const;
};

class UnscheduledEvents2 : public CostComponent<TT_Input,TT_State>
{
public:
  UnscheduledEvents2(const TT_Input& in, int w, bool hard) : 
    CostComponent<TT_Input,TT_State>(in,w,hard,"UnscheduledEvents2")
  {}
  int ComputeCost(const TT_State& st) const;
  void PrintViolations(const TT_State& st, std::ostream& os = std::cout) const;
};

class StudentLastSlot : public CostComponent<TT_Input,TT_State>
{
public:
  StudentLastSlot(const TT_Input& in, int w, bool hard) : 
    CostComponent<TT_Input,TT_State>(in,w,hard,"Student Last Slot") {}
  int ComputeCost(const TT_State& st) const;
  void PrintViolations(const TT_State& st, std::ostream& os = std::cout) const;
};

class StudentClassRow : public CostComponent<TT_Input,TT_State>
{
public:
  StudentClassRow(const TT_Input& in, int w, bool hard) : 
    CostComponent<TT_Input,TT_State>(in,w,hard,"Student Class Row") {}
  int ComputeCost(const TT_State& st) const;
  void PrintViolations(const TT_State& st, std::ostream& os = std::cout) const;
};

class StudentOneClassPerDay : public CostComponent<TT_Input,TT_State>
{
public:
  StudentOneClassPerDay(const TT_Input& in, int w, bool hard) : 
    CostComponent<TT_Input,TT_State>(in,w,hard,"Student One Class Per Day") {}
  int ComputeCost(const TT_State& st) const;
  void PrintViolations(const TT_State& st, std::ostream& os = std::cout) const;
};


/***************************************************************************
 * State Manager
 ***************************************************************************/

class TT_StateManager
  : public StateManager<TT_Input,TT_State> 
{
public:
  TT_StateManager(const TT_Input&);
  void RandomState(TT_State&);
  void GreedyState(TT_State&);
  unsigned StateDistance(const TT_State& st1, const TT_State& st2)  const;
  void PrintStateDistance(const TT_State& st1, const TT_State& st2, ostream& os = cout) const {}
  unsigned RoomDistance(const TT_State& st1, const TT_State& st2)  const;
  bool CheckConsistency(const TT_State& st) const  { return st.Check(); }
}; 

/***************************************************************************
 * Output Manager 
 ***************************************************************************/

class TT_OutputManager
  : public OutputManager<TT_Input,TT_Output,TT_State> 
{
public:
  TT_OutputManager(const TT_Input& pin)
    : OutputManager<TT_Input,TT_Output,TT_State>(pin,"TT_OutputManager") {}
  void InputState(TT_State&, const TT_Output&) const;
  void OutputState(const TT_State&, TT_Output&) const;
private:
};


/***************************************************************************
 * ME_Neighborhood Explorer and related stuff
 ***************************************************************************/

class MEE_NeighborhoodExplorer
  : public NeighborhoodExplorer<TT_Input,TT_State,MoveEvent> 
{
public:
  MEE_NeighborhoodExplorer(const TT_Input&, StateManager<TT_Input,TT_State>&);
  void RandomMove(const TT_State&, MoveEvent&) const;
  void MakeMove(TT_State&, const MoveEvent&) const;
  bool FeasibleMove(const TT_State& st, const MoveEvent& mv) const;
  bool NextMove(const TT_State&, MoveEvent&) const;
  void FirstMove(const TT_State&, MoveEvent&) const;
protected:
  bool FeasibleNewSlot(const TT_State& st, MoveEvent& mv) const;
private:
  bool AnyNextMove(const TT_State&,MoveEvent&) const;
  void AnyRandomMove(const TT_State&, MoveEvent&) const;
  bool NextEvent(const TT_State& st, MoveEvent& mv) const;
  bool NextNewSlot(const TT_State& st, MoveEvent& mv) const;
  bool FirstNewSlot(const TT_State& st, MoveEvent& mv) const;
  virtual bool FindRoom(const TT_State& st, MoveEvent& mv) const;
};
  
class ME_NeighborhoodExplorer : public MEE_NeighborhoodExplorer
{ // no unscheduled events (can be implemented more efficiently, removing usncheduling from NHE and DCCs)
public:
  ME_NeighborhoodExplorer(const TT_Input& in, StateManager<TT_Input,TT_State>& sm) : MEE_NeighborhoodExplorer(in,sm) {}
  bool FeasibleMove(const TT_State& st, const MoveEvent& mv) const;
  void RandomMove(const TT_State&, MoveEvent&) const;
}; 
 
class ME_DeltaStudentClash : public FilledDeltaCostComponent<TT_Input,TT_State,MoveEvent>
{
public:
  ME_DeltaStudentClash(const TT_Input& in, StudentClash& cc) : 
    FilledDeltaCostComponent<TT_Input,TT_State,MoveEvent>(in,cc,"ME_DeltaStudentClash") {}
  int ComputeDeltaCost(const TT_State& st, const MoveEvent& mv) const;
};

class ME_DeltaStudentClash2 : public FilledDeltaCostComponent<TT_Input,TT_State,MoveEvent>
{
public:
  ME_DeltaStudentClash2(const TT_Input& in, StudentClash2& cc) : 
    FilledDeltaCostComponent<TT_Input,TT_State,MoveEvent>(in,cc,"ME_DeltaStudentClash2") {}
  int ComputeDeltaCost(const TT_State& st, const MoveEvent& mv) const;
};

class ME_DeltaEventPrecedence : public FilledDeltaCostComponent<TT_Input,TT_State,MoveEvent>
{
public:
  ME_DeltaEventPrecedence(const TT_Input& in, EventPrecedence& cc) : 
    FilledDeltaCostComponent<TT_Input,TT_State,MoveEvent>(in,cc,"ME_DeltaEventPrecedence") {}
  int ComputeDeltaCost(const TT_State& st, const MoveEvent& mv) const;
};

class ME_DeltaUnscheduledEvents : public FilledDeltaCostComponent<TT_Input,TT_State,MoveEvent>
{
public:
  ME_DeltaUnscheduledEvents(const TT_Input& in, UnscheduledEvents& cc) : 
    FilledDeltaCostComponent<TT_Input,TT_State,MoveEvent>(in,cc,"ME_DeltaUnscheduledEvents") {}
  int ComputeDeltaCost(const TT_State& st, const MoveEvent& mv) const;
};

class ME_DeltaUnscheduledEvents2 : public FilledDeltaCostComponent<TT_Input,TT_State,MoveEvent>
{
public:
  ME_DeltaUnscheduledEvents2(const TT_Input& in, UnscheduledEvents2& cc) : 
    FilledDeltaCostComponent<TT_Input,TT_State,MoveEvent>(in,cc,"ME_DeltaUnscheduledEvents2") {}
  int ComputeDeltaCost(const TT_State& st, const MoveEvent& mv) const;
};

class ME_DeltaStudentLastSlot : public FilledDeltaCostComponent<TT_Input,TT_State,MoveEvent>
{
public:
  ME_DeltaStudentLastSlot(const TT_Input& in, StudentLastSlot& cc) : 
    FilledDeltaCostComponent<TT_Input,TT_State,MoveEvent>(in,cc,"ME_DeltaStudentLastSlot") {}
  int ComputeDeltaCost(const TT_State& st, const MoveEvent& mv) const;
};

class ME_DeltaStudentClassRow : public FilledDeltaCostComponent<TT_Input,TT_State,MoveEvent>
{
public:
  ME_DeltaStudentClassRow(const TT_Input& in, StudentClassRow& cc) : 
    FilledDeltaCostComponent<TT_Input,TT_State,MoveEvent>(in,cc,"ME_DeltaStudentClassRow") {}
  int ComputeDeltaCost(const TT_State& st, const MoveEvent& mv) const;
};

class ME_DeltaStudentOneClassPerDay : public FilledDeltaCostComponent<TT_Input,TT_State,MoveEvent>
{
public:
  ME_DeltaStudentOneClassPerDay(const TT_Input& in, StudentOneClassPerDay& cc) : 
    FilledDeltaCostComponent<TT_Input,TT_State,MoveEvent>(in,cc,"ME_DeltaStudentOneClassPerDay") {}
  int ComputeDeltaCost(const TT_State& st, const MoveEvent& mv) const;
};

/***************************************************************************
 * SwapTime Neighborhood Explorer and related stuff
 ***************************************************************************/
 
class SwapTime_NeighborhoodExplorer
  : public NeighborhoodExplorer<TT_Input,TT_State,SwapTime> 
{
public:
  SwapTime_NeighborhoodExplorer(const TT_Input&, StateManager<TT_Input,TT_State>&);
  void RandomMove(const TT_State&, SwapTime&) const;
  void MakeMove(TT_State&, const SwapTime&) const;
  bool FeasibleMove(const TT_State& st, const SwapTime& mv) const;
  //protected:
  bool NextMove(const TT_State&,SwapTime&) const;
  void FirstMove(const TT_State&,SwapTime&) const;
private:
  bool AnyNextMove(const TT_State&,SwapTime&) const;
};
  
class SwapTime_NeighborhoodExplorerS
  : public SwapTime_NeighborhoodExplorer
{
public:
  SwapTime_NeighborhoodExplorerS(const TT_Input& in, StateManager<TT_Input,TT_State>& sm)
    : SwapTime_NeighborhoodExplorer(in,sm) {}
  void RandomMove(const TT_State&, SwapTime&) const;
  bool FeasibleMove(const TT_State& st, const SwapTime& mv) const;
  bool FeasibleMoveSlots(const TT_State& st, const SwapTime& mv) const;
};
  
class SwapTimeDeltaStudentClash : public FilledDeltaCostComponent<TT_Input,TT_State,SwapTime>
{
public:
  SwapTimeDeltaStudentClash(const TT_Input& in, StudentClash& cc) : 
    FilledDeltaCostComponent<TT_Input,TT_State,SwapTime>(in,cc,"SwapTimeDeltaStudentClash") {}
  int ComputeDeltaCost(const TT_State& st, const SwapTime& mv) const;
};

class SwapTimeDeltaStudentClash2 : public FilledDeltaCostComponent<TT_Input,TT_State,SwapTime>
{
public:
  SwapTimeDeltaStudentClash2(const TT_Input& in, StudentClash2& cc) : 
    FilledDeltaCostComponent<TT_Input,TT_State,SwapTime>(in,cc,"SwapTimeDeltaStudentClash2") {}
  int ComputeDeltaCost(const TT_State& st, const SwapTime& mv) const;
};

class SwapTimeDeltaEventPrecedence : public FilledDeltaCostComponent<TT_Input,TT_State,SwapTime>
{
public:
  SwapTimeDeltaEventPrecedence(const TT_Input& in, EventPrecedence& cc) : 
    FilledDeltaCostComponent<TT_Input,TT_State,SwapTime>(in,cc,"SwapTimeDeltaEventPrecedence") {}
  int ComputeDeltaCost(const TT_State& st, const SwapTime& mv) const;
};

class SwapTimeDeltaUnscheduledEvents : public FilledDeltaCostComponent<TT_Input,TT_State,SwapTime>
{
public:
  SwapTimeDeltaUnscheduledEvents(const TT_Input& in, UnscheduledEvents& cc) : 
    FilledDeltaCostComponent<TT_Input,TT_State,SwapTime>(in,cc,"SwapTimeDeltaUnscheduledEvents") {}
  int ComputeDeltaCost(const TT_State& st, const SwapTime& mv) const;
};

//class SwapTimeDeltaUnscheduledEvents2: no change in this case

class SwapTimeDeltaStudentLastSlot : public FilledDeltaCostComponent<TT_Input,TT_State,SwapTime>
{
public:
  SwapTimeDeltaStudentLastSlot(const TT_Input& in, StudentLastSlot& cc) : 
    FilledDeltaCostComponent<TT_Input,TT_State,SwapTime>(in,cc,"SwapTimeDeltaStudentLastSlot") {}
  int ComputeDeltaCost(const TT_State& st, const SwapTime& mv) const;
};

class SwapTimeDeltaStudentClassRow : public FilledDeltaCostComponent<TT_Input,TT_State,SwapTime>
{
public:
  SwapTimeDeltaStudentClassRow(const TT_Input& in, StudentClassRow& cc) : 
    FilledDeltaCostComponent<TT_Input,TT_State,SwapTime>(in,cc,"SwapTimeDeltaStudentClassRow") {}
  int ComputeDeltaCost(const TT_State& st, const SwapTime& mv) const;
};

class SwapTimeDeltaStudentOneClassPerDay : public FilledDeltaCostComponent<TT_Input,TT_State,SwapTime>
{
public:
  SwapTimeDeltaStudentOneClassPerDay(const TT_Input& in, StudentOneClassPerDay& cc) : 
    FilledDeltaCostComponent<TT_Input,TT_State,SwapTime>(in,cc,"SwapTimeDeltaStudentOneClassPerDay") {}
  int ComputeDeltaCost(const TT_State& st, const SwapTime& mv) const;
};

#endif
