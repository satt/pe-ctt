// File TT_Runners.hh
#ifndef ICT07_2_RUNNERS_H
#define ICT07_2_RUNNERS_H

#include <iostream>
#include "TT_Helpers.hh"
#include "Kickers.hh"

class TT_Kicker : public BimodalKicker<TT_Input,TT_State,MoveEvent,SwapTime>
{
public:
  TT_Kicker(const TT_Input& in, ME_NeighborhoodExplorer& bnhe, SwapTime_NeighborhoodExplorer& stnhe);
  bool RelatedMoves(const SwapTime &mv1, const SwapTime &mv2) const; 
  bool RelatedMoves(const SwapTime &mv1, const MoveEvent &mv2) const; 
  bool RelatedMoves(const MoveEvent &mv1, const SwapTime &mv2) const; 
  bool RelatedMoves(const MoveEvent &mv1, const MoveEvent &mv2) const; 
};

#endif
