// File TT_Runners.cc
#include "TT_Runners.hh"


TT_Kicker::TT_Kicker(const TT_Input& in, ME_NeighborhoodExplorer& bnhe, SwapTime_NeighborhoodExplorer& stnhe)
  : BimodalKicker<TT_Input,TT_State,MoveEvent,SwapTime>(in,bnhe,stnhe,2,"Bimodal Kicker") {}


bool TT_Kicker::RelatedMoves(const SwapTime &mv1, const SwapTime &mv2) const
{
  return mv1.slot2 == mv2.slot1;
}
 
bool TT_Kicker::RelatedMoves(const SwapTime &mv1, const MoveEvent &mv2) const
{
  return mv1.slot1 == mv2.old_slot || mv1.slot2 == mv2.old_slot;
}
 
bool TT_Kicker::RelatedMoves(const MoveEvent &mv1, const SwapTime &mv2) const
{
  return mv1.new_slot == mv2.slot1 || mv1.new_slot == mv2.slot1;
}
 
bool TT_Kicker::RelatedMoves(const MoveEvent &mv1, const MoveEvent &mv2) const
{
  return mv1.new_slot == mv2.old_slot && mv1.event != mv2.event;
}
 
