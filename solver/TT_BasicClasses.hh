// File BasicClasses.h
#ifndef ICT07_2_BASIC_CLASSES_HH
#define ICT07_2_BASIC_CLASSES_HH

#include <vector>
#include <map>
#include <list>
#include <string>

using namespace std;


/***************************************************************************
 * Input class
 ***************************************************************************/

class TT_Input
{
  friend ostream& operator<<(ostream& os, const TT_Input& in);
public:
  TT_Input(string file_name, bool old_format);
    // constant values
  const unsigned TIMESLOTS;
  const unsigned DAYS;
  const unsigned DAY_SLOTS;
     // dummy values
  const unsigned UNSCHEDULED; // assign this value (-1) when an event is unscheduled
  unsigned ANY_ROOM; // assign this value (rooms) when an event can go in any room, and is assigned later
  unsigned DUMMY_ROOM; // this value is returned by some function to state that no room is available for the event
  const int NO_EVENT; // this value is stored in some data structure to state that no event is present 

  unsigned Rooms() const { return rooms; }
  unsigned Events() const { return events; }
  unsigned Students() const { return students; }

  bool StudentEventEnrolment(unsigned s, unsigned e) const  { return student_event_enrolment[s][e]; }
  bool EventRoomCompatibility(unsigned e, unsigned r) const  { return event_room_compatibility[e][r]; }
  unsigned EventRoomList(unsigned e, unsigned a) const { return event_room_list[e][a]; }
  unsigned EventStudentList(unsigned e, unsigned a) const { return event_student_list[e][a]; }
  unsigned EventRoomNumber(unsigned e) const  { return event_room_list[e].size(); }
  unsigned EventStudentNumber(unsigned e) const  { return event_event_conflict[e][e]; }
  unsigned EventConflict(unsigned e1, unsigned e2) const  { return event_event_conflict[e1][e2]; }
  bool EventPrecedence(unsigned e1, unsigned e2) const { return event_event_precedence[e1][e2]; }
  bool EventPeriodAvailability(unsigned e, unsigned p) const { return event_period_availability[e][p]; }
  unsigned  EventBeforeNumber(unsigned e) const { return event_before_list[e].size(); }
  unsigned  EventAfterNumber(unsigned e) const { return event_after_list[e].size(); }
  unsigned  EventBeforeList(unsigned e, unsigned a) const { return event_before_list[e][a]; }
  unsigned  EventAfterList(unsigned e, unsigned a) const { return event_after_list[e][a]; }
  bool AnyRoomEvent(unsigned e) const { return anyroom_event[e]; }

  // statistics
  float WeightedDensity() const { return weighted_density; }
  float WeightedConflicts() const { return weighted_conflicts; }
  float Density() const { return density; }
  float Conflicts() const { return conflicts; }

private:
  unsigned rooms, events, students;
  vector<vector<bool> > student_event_enrolment;
  vector<vector<bool> > event_room_compatibility;
  vector<vector<bool> > event_event_precedence; 
  vector<vector<bool> > event_period_availability; 

  // redundant data structures (for efficiency)
  vector<vector<unsigned> > event_room_list; 
  vector<vector<unsigned> > event_student_list; 
  vector<vector<unsigned> > event_event_conflict;

  vector<vector<unsigned> > event_before_list; // redundant: another view of event_event_precedence
  vector<vector<unsigned> > event_after_list; // redundant:  "

  vector<bool> anyroom_event;
  float weighted_density, weighted_conflicts;
  float density, conflicts;
};

ostream& operator<<(ostream& os, const TT_Input& in);

/***************************************************************************
 * Output class
 ***************************************************************************/

class TT_Output
{
  friend ostream& operator<<(ostream& os, const TT_Output& in);
  friend istream& operator>>(istream& is, TT_Output& in);
public:
  TT_Output(const TT_Input& in);
  TT_Output(const TT_Output& out);
  TT_Output& operator=(const TT_Output& out);
private:
  const TT_Input& in;
public: // public only for simplicity (should be private, with suitable selectors)
  vector<unsigned> timeslot;  // timeslot = in.TIMESLOTS --> unassigned event
  vector<unsigned> room;      
};

ostream& operator<<(ostream& os, const TT_Output& out);
istream& operator>>(istream& is, TT_Output& out);

/***************************************************************************
 * State class
 ***************************************************************************/

class TT_State
{
  friend bool operator==(const TT_State& st1, const TT_State& st2);
  friend ostream& operator<<(ostream& os, const TT_State& st);
public:
  TT_State(const TT_Input& in);
  TT_State(const TT_State& s);
  TT_State& operator=(const TT_State& s);

  bool Check() const;

  unsigned FindFreeRoom(unsigned e, unsigned ts) const;
  unsigned FindFreeRoom(unsigned e, unsigned ts, unsigned r) const;
  void ClearRedundantData();

  void UpdateTimeSlotRedundantData();
  void UpdateAdditionalRedundantData();
  void UpdateRoomRedundantData();
  void UpdateRedundantData();
  void AssignAllRooms();  

  // dedicated to manage slot_event array 
  void InsertSlotEvent(unsigned ts, unsigned e);
  void RemoveSlotEvent(unsigned ts, unsigned e);
  void ReplaceSlotEvent(unsigned ts, unsigned e1, unsigned e2);

  // dedicated to manage slot_room_event array and compute the RoomClash cost
//   void InsertSlotRoomEvent(unsigned ts, unsigned r, unsigned e);
//   void RemoveSlotRoomEvent(unsigned ts, unsigned r, unsigned e);
//   int ReplaceSlotRoomEventCost(unsigned ts, unsigned r, unsigned e1, unsigned e2) const;
//   unsigned InsertSlotRoomEventCost(unsigned ts, unsigned r, unsigned e) const;
//   unsigned RemoveSlotRoomEventCost(unsigned ts, unsigned r, unsigned e) const;

  bool SlotRoomFree(unsigned ts, unsigned r) const 
  { return slot_room_event[ts][r] == in.NO_EVENT; }

  // dedicated to manage student_daily_events
  void IncreaseStudentDailyEvents(unsigned e, unsigned d);
  void DecreaseStudentDailyEvents(unsigned e, unsigned d);
  void TransferStudentDailyEvents(unsigned e, unsigned d1, unsigned d2);

  // dedicated to manage student_triplet_events
  void IncreaseStudentTripletEvents(unsigned e, unsigned ts);
  void DecreaseStudentTripletEvents(unsigned e, unsigned ts);

  int ReplaceEventTripletCost(unsigned e1, unsigned e2, unsigned ts) const;
  unsigned RemoveEventTripletCost(unsigned e, unsigned ts) const;
  unsigned InsertEventTripletCost(unsigned e, unsigned ts) const;
  unsigned TripletCost(unsigned e, unsigned ts) const;
  unsigned TripletCost(unsigned ts, unsigned e1, unsigned e2) const;

  // dedicated to manage clash costs 
  unsigned StudentClashCost(unsigned e, unsigned ts) const;
  unsigned StudentClashCost2(unsigned e, unsigned ts) const;
  unsigned StudentClashCost(unsigned e, unsigned ts, unsigned e_excluded) const;

  // dedicated to manage precedence costs 
  unsigned EventPrecedenceCost(unsigned e, unsigned ts) const;
  int OneClassPerDayTransferCost(unsigned e, unsigned d1, unsigned d2, unsigned e_r) const;
  int OneClassPerDayInsertCost(unsigned e, unsigned d, unsigned e_r) const;
  int OneClassPerDayRemoveCost(unsigned e, unsigned d, unsigned e_r) const;

private:

  const TT_Input& in;

public: // public only for simplicity (should be private, with suitable selectors)
  typedef list<unsigned> EventList;
  vector<unsigned> timeslot;    // timeslot = in.TIMESLOTS --> unassigned event
  vector<unsigned> room;        // room = in.Rooms() --> assigned to any room, room = in.Rooms() + 1 assigned by matching
  vector<EventList> all_slot_events; // all events in the slot
  vector<EventList> roomed_slot_events; // events that need a room
  vector<vector<int> > slot_room_event;
  vector<vector<unsigned> > student_daily_events;
  vector<vector<unsigned> > student_triplet_events; // events in the slots (i-1, i, i+1)
};

ostream& operator<<(ostream& os, const TT_State& st);

/***************************************************************************
 * Move classes
 ***************************************************************************/

class MoveEvent
{
public:
  unsigned event;
  unsigned old_room;
  unsigned new_room;
  unsigned old_slot;
  unsigned new_slot;
  unsigned room_index;  // index of new_room in p_in->event_room_list[event]
                        // needed to make NextMove() more efficient
};

inline bool operator==(const MoveEvent& mv1, const MoveEvent& mv2)
{ return mv1.event == mv2.event && mv1.old_slot == mv2.old_slot && mv1.new_slot == mv2.new_slot
    && mv1.old_room == mv2.old_room && mv1.new_room == mv2.new_room; }

inline  bool operator!=(const MoveEvent& mv1, const MoveEvent& mv2)
{ return mv1.event != mv2.event || mv1.old_slot != mv2.old_slot || mv1.new_slot != mv2.new_slot
    || mv1.old_room != mv2.old_room || mv1.new_room != mv2.new_room; }

ostream& operator<<(ostream& os, const MoveEvent& mv);
istream& operator>>(istream& is, MoveEvent& mv);
bool operator<(const MoveEvent& mv1, const MoveEvent& mv2);

class SwapTimeAndRoom
{
public:
  unsigned event1;
  unsigned event2;
  unsigned slot1;
  unsigned slot2;
  unsigned room1;
  unsigned room2;
};

inline bool operator==(const SwapTimeAndRoom& mv1, const SwapTimeAndRoom& mv2)
  { return mv1.event1 == mv2.event1 && mv1.event2 == mv2.event2 
      && mv1.slot1 == mv2.slot1 && mv1.slot2 == mv2.slot2; }

inline bool operator!=(const SwapTimeAndRoom& mv1, const SwapTimeAndRoom& mv2)
  { return mv1.event1 != mv2.event1 || mv1.event2 != mv2.event2 
      || mv1.slot1 != mv2.slot1 || mv1.slot2 != mv2.slot2; }

ostream& operator<<(ostream& os, const SwapTimeAndRoom& mv);
istream& operator>>(istream& is, SwapTimeAndRoom& mv);
bool operator<(const SwapTimeAndRoom& mv1, const SwapTimeAndRoom& mv2);

class SwapTime
{
public:
  unsigned event1;
  unsigned event2;
  unsigned slot1;
  unsigned slot2;
  unsigned old_room1, new_room1;
  unsigned old_room2, new_room2;
};

inline bool operator==(const SwapTime& mv1, const SwapTime& mv2)
{ return mv1.event1 == mv2.event1 && mv1.event2 == mv2.event2 
    && mv1.slot1 == mv2.slot1 && mv1.slot2 == mv2.slot2; }

inline bool operator!=(const SwapTime& mv1, const SwapTime& mv2)
{ return mv1.event1 != mv2.event1 || mv1.event2 != mv2.event2 
    || mv1.slot1 != mv2.slot1 || mv1.slot2 != mv2.slot2; }

ostream& operator<<(ostream& os, const SwapTime& mv);
istream& operator>>(istream& is, SwapTime& mv);
bool operator<(const SwapTime& mv1, const SwapTime& mv2);

#endif
