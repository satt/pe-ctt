// File Helpers.cpp

#include "TT_Helpers.hh"
#include <fstream>

/***************************************************************************
 * Cost Components
 ***************************************************************************/

int StudentClash::ComputeCost(const TT_State& st) const
{
  int count = 0;
  for (unsigned ts = 0; ts < in.TIMESLOTS; ts++)
    {
      if (st.all_slot_events[ts].size() > 1)
	{
	  for (TT_State::EventList::const_iterator e1 = st.all_slot_events[ts].begin(); e1 != st.all_slot_events[ts].end(); e1++)
	    {
	      TT_State::EventList::const_iterator e2 = e1;
	      e2++;
	      while (e2 != st.all_slot_events[ts].end())	  
		{
		  if (in.EventConflict(*e1, *e2) > 0)
		    // The cost is the min() because it should be equal to unschedule the least expensive
		    count += min(in.EventStudentNumber(*e1),in.EventStudentNumber(*e2)); 
		  e2++;
		}
	    }
	}
    }
  return count;
}

void StudentClash::PrintViolations(const TT_State& st, std::ostream& os) const
{
  for (unsigned ts = 0; ts < in.TIMESLOTS; ts++)
    {
      if (st.all_slot_events[ts].size() > 1)
	{
	  for (TT_State::EventList::const_iterator e1 = st.all_slot_events[ts].begin(); e1 != st.all_slot_events[ts].end(); e1++)
	    {
	      TT_State::EventList::const_iterator e2 = e1;
	      e2++;
	      while (e2 != st.all_slot_events[ts].end())	  		
		{		  
		  if (in.EventConflict(*e1, *e2) > 0)
		    os << "There are " << in.EventConflict(*e1, *e2) 
		       << " students taking both events " << *e1 << " (" << in.EventStudentNumber(*e1) 
		       << ") and " << *e2 << " (" << in.EventStudentNumber(*e2) 
		       << ") at timeslot " << ts << endl;
		  e2++;
		}
	    }
	}
    }
}

int StudentClash2::ComputeCost(const TT_State& st) const
{
  int count = 0;
  for (unsigned ts = 0; ts < in.TIMESLOTS; ts++)
    {
      if (st.all_slot_events[ts].size() > 1)
	{
	  for (TT_State::EventList::const_iterator e1 = st.all_slot_events[ts].begin(); e1 != st.all_slot_events[ts].end(); e1++)
	    {
	      TT_State::EventList::const_iterator e2 = e1;
	      e2++;
	      while (e2 != st.all_slot_events[ts].end())	  
		{
		  if (in.EventConflict(*e1, *e2) > 0)
		    count++;
		  e2++;
		}
	    }
	}
    }
  return count;
}

void StudentClash2::PrintViolations(const TT_State& st, std::ostream& os) const
{
  for (unsigned ts = 0; ts < in.TIMESLOTS; ts++)
    {
      if (st.all_slot_events[ts].size() > 1)
	{
	  for (TT_State::EventList::const_iterator e1 = st.all_slot_events[ts].begin(); e1 != st.all_slot_events[ts].end(); e1++)
	    {
	      TT_State::EventList::const_iterator e2 = e1;
	      e2++;
	      while (e2 != st.all_slot_events[ts].end())	  		
		{		  
		  if (in.EventConflict(*e1, *e2) > 0)
		    os << "There are " << in.EventConflict(*e1, *e2) 
		       << " students taking both events " << *e1 << " (" << in.EventStudentNumber(*e1) 
		       << ") and " << *e2 << " (" << in.EventStudentNumber(*e2) 
		       << ") at timeslot " << ts << endl;
		  e2++;
		}
	    }
	}
    }
}

int EventPrecedence::ComputeCost(const TT_State& st) const
{
  int count = 0;
  for (unsigned e1 = 0; e1 < in.Events(); e1++)
    {
      if (st.timeslot[e1] != in.UNSCHEDULED) // the exam is scheduled
	for (unsigned a = 0; a < in.EventBeforeNumber(e1); a++)
	  {
	    unsigned e2 = in.EventBeforeList(e1,a);
	    if (st.timeslot[e1] <= st.timeslot[e2] && st.timeslot[e2] != in.TIMESLOTS)
	      count += min(in.EventStudentNumber(e1), in.EventStudentNumber(e2));
	  }
    }
  return count;
}

void EventPrecedence::PrintViolations(const TT_State& st, std::ostream& os) const
{
  for (unsigned e1 = 0; e1 < in.Events(); e1++)
    {
      if (st.timeslot[e1] != in.UNSCHEDULED) // the exam is scheduled
	for (unsigned a = 0; a < in.EventBeforeNumber(e1); a++)
	  {
	    unsigned e2 = in.EventBeforeList(e1,a);
	    if (st.timeslot[e1] <= st.timeslot[e2] && st.timeslot[e2] != in.TIMESLOTS)
	      os << "Event " << e2 << " (" <<  in.EventStudentNumber(e2) 
		 << ") is not before event " << e1 << " (" << in.EventStudentNumber(e1) << ")" << endl;
	  }
    }
}

int UnscheduledEvents::ComputeCost(const TT_State& st) const
{
  int count = 0;
  for (unsigned e = 0; e < in.Events(); e++)
    {
      if (st.timeslot[e] == in.UNSCHEDULED) // the event is unscheduled
	count += in.EventStudentNumber(e);
    }
  return count;
}

void UnscheduledEvents::PrintViolations(const TT_State& st, std::ostream& os) const
{
  for (unsigned e = 0; e < in.Events(); e++)
    {
      if (st.timeslot[e] == in.UNSCHEDULED) // the event is scheduled
	 os << "Event " << e << " (" <<  in.EventStudentNumber(e) 
	    << ") is unscheduled" << endl;
    }
}

int UnscheduledEvents2::ComputeCost(const TT_State& st) const
{
  int count = 0;
  for (unsigned e = 0; e < in.Events(); e++)
    {
      if (st.timeslot[e] == in.UNSCHEDULED) // the event is unscheduled
	count++;
    }
  return count;
}

void UnscheduledEvents2::PrintViolations(const TT_State& st, std::ostream& os) const
{
  for (unsigned e = 0; e < in.Events(); e++)
    {
      if (st.timeslot[e] == in.UNSCHEDULED) // the event is scheduled
	 os << "Event " << e << " (" <<  in.EventStudentNumber(e) 
	    << ") is unscheduled" << endl;
    }
}

int StudentLastSlot::ComputeCost(const TT_State& st) const
{
  int count = 0;
  for (unsigned ts = in.DAY_SLOTS-1; ts < in.TIMESLOTS; ts += in.DAY_SLOTS)
    for (TT_State::EventList::const_iterator a = st.all_slot_events[ts].begin(); a != st.all_slot_events[ts].end(); a++)
	count += in.EventStudentNumber(*a);

  //cerr << name << ' ' << count << endl;
  return count;
}

void StudentLastSlot::PrintViolations(const TT_State& st, std::ostream& os) const
{
  for (unsigned ts = in.DAY_SLOTS-1; ts < in.TIMESLOTS; ts += in.DAY_SLOTS)
    for (TT_State::EventList::const_iterator a = st.all_slot_events[ts].begin(); a != st.all_slot_events[ts].end(); a++)
	if (*a > 0 && in.EventStudentNumber(*a) > 0)
	  os << "There are " << in.EventStudentNumber(*a) << " students taking event " 
	     << *a << " at timeslot " << ts << endl;
}

int StudentClassRow::ComputeCost(const TT_State& st) const
{
  int count = 0;

  for (unsigned s = 0; s < in.Students(); s++)
    for (unsigned ts = 0; ts < in.TIMESLOTS; ts++)
      if (ts % in.DAY_SLOTS != 0 && ts % in.DAY_SLOTS != in.DAY_SLOTS - 1)
	if (st.student_triplet_events[s][ts] >= 3)
	  count += st.student_triplet_events[s][ts] - 2;
  //cerr << name << ' ' << count << endl;
  return count;
}

void StudentClassRow::PrintViolations(const TT_State& st, std::ostream& os) const
{
 for (unsigned s = 0; s < in.Students(); s++)
    for (unsigned ts = 0; ts < in.TIMESLOTS; ts++)
      if (ts % in.DAY_SLOTS != 0 && ts % in.DAY_SLOTS != in.DAY_SLOTS - 1)
	if (st.student_triplet_events[s][ts] >= 3)
	  cout << "Student " << s << " has three events in a row ending at period " 
	       << ts+1 << endl;
}

int StudentOneClassPerDay::ComputeCost(const TT_State& st) const
{
  int count = 0;
  unsigned s, d;

  for (s = 0; s < in.Students(); s++)
    for (d = 0; d < in.DAYS; d++)
      if (st.student_daily_events[s][d] == 1)
	count++;
  //cerr << name << ' ' << count << endl;
  return count;
}

void StudentOneClassPerDay::PrintViolations(const TT_State& st, std::ostream& os) const
{
  unsigned s, d;
  for (s = 0; s < in.Students(); s++)
    for (d = 0; d < in.DAYS; d++)
      if (st.student_daily_events[s][d] == 1)
	os << "Student " << s << " has only one event in day " << d << endl;
}

/***************************************************************************
 * State Manager 
 ***************************************************************************/

TT_StateManager::TT_StateManager(const TT_Input& pin)
  :  StateManager<TT_Input,TT_State>(pin,"TT_StateManager") {}

void TT_StateManager::GreedyState(TT_State& st)
{ // come randomstate ma con tutti gli eventi schedulati (se possibile)
  unsigned e, r, ri = 0, ts;  
  unsigned random_trials;

  // clear old data
  st.ClearRedundantData();
	
  for(e = 0; e < in.Events(); e++)
    {
      random_trials = 0; 
      do
	{
	  ts = Random::Int(0,in.TIMESLOTS-1); 	 
	  random_trials++;
	} 
      while (random_trials < 2 * in.TIMESLOTS && (!in.EventPeriodAvailability(e, ts) || st.all_slot_events[ts].size() == in.Rooms()
						  || (!in.AnyRoomEvent(e)  && st.FindFreeRoom(e,ts) == in.DUMMY_ROOM)));
      if (in.AnyRoomEvent(e))
	r = in.ANY_ROOM;
      else
	{
	  ri = st.FindFreeRoom(e,ts);
	  if (ri < in.Rooms()) // in.ANY_ROOM and in.DUMMY_ROOM are bigger or equal to in.Rooms()
	    r = in.EventRoomList(e,ri);
	  else
	    {
	      ts = in.UNSCHEDULED;
	      r = in.ANY_ROOM;
	    }
	}
      st.timeslot[e] = ts;
      st.room[e] = r;
      if (ts != in.UNSCHEDULED)
	{
	  st.all_slot_events[ts].push_back(e);
	  if (!in.AnyRoomEvent(e))
	    st.roomed_slot_events[ts].push_back(e);
	  st.slot_room_event[ts][r] = e;
	}
    }
	
  st.UpdateAdditionalRedundantData();
  //  st.Check();
}

void TT_StateManager::RandomState(TT_State& st)
{
  unsigned e, r, ri = 0, ts;  

  // clear old data
  st.ClearRedundantData();
	
  for(e = 0; e < in.Events(); e++)
    {
      do
	{
	  ts = Random::Int(0,in.TIMESLOTS-1); 
	} 
      while (!in.EventPeriodAvailability(e, ts) || st.all_slot_events[ts].size() == in.Rooms());

      if (in.AnyRoomEvent(e))
	r = in.Rooms();
      else
	{
	  ri = st.FindFreeRoom(e,ts);
	  if (ri < in.Rooms())
	    r = in.EventRoomList(e,ri);
	  else
	    {
	      ts = in.UNSCHEDULED;
	      r = in.Rooms();
	    }
	}
      st.timeslot[e] = ts;
      st.room[e] = r;
      if (ts != in.UNSCHEDULED)
	{
	  st.all_slot_events[ts].push_back(e);
	  if (!in.AnyRoomEvent(e))
	    st.roomed_slot_events[ts].push_back(e);
	  st.slot_room_event[ts][r] = e;
	}
    }
	
  st.UpdateAdditionalRedundantData();
  //  st.Check();
}

unsigned TT_StateManager::StateDistance(const TT_State& st1, const TT_State& st2)  const 
{ 
  unsigned distance = 0;
  for(unsigned e = 0; e < in.Events(); e++)
    if (st1.timeslot[e] != st2.timeslot[e])
      distance++;
  return distance;
}

unsigned TT_StateManager::RoomDistance(const TT_State& st1, const TT_State& st2)  const 
{ 
  unsigned distance = 0;
  for(unsigned e = 0; e < in.Events(); e++)
    if (st1.room[e] != st2.room[e])
      distance++;
  return distance;
}


/***************************************************************************
 * Output Manager 
 ***************************************************************************/

void TT_OutputManager::InputState(TT_State& st, const TT_Output& out) const
{
  unsigned e;
  st.timeslot = out.timeslot;
  for (e = 0; e < in.Events(); e++)
    if (in.AnyRoomEvent(e))
      st.room[e] = in.Rooms(); // put it in the virtual room
    else
      st.room[e] = out.room[e];
  st.UpdateRedundantData();
}

void TT_OutputManager::OutputState(const TT_State& st, TT_Output& out) const
{
  unsigned ts, r, e;
  vector<vector<bool> > place_anyroom_event(in.TIMESLOTS,vector<bool>(in.Rooms(),false));

  out.timeslot = st.timeslot;
  for (e = 0; e < in.Events(); e++)
    if (in.AnyRoomEvent(e))
      {	
	ts = st.timeslot[e];
	if (ts == in.UNSCHEDULED)
	  out.room[e] = in.Rooms();
	else
	  for (r = 0; r < in.Rooms(); r++)	
	    {
	      if (st.SlotRoomFree(ts,r) && !place_anyroom_event[ts][r])
		{
		  place_anyroom_event[ts][r] = true;
		  out.room[e] = r;
		  break;

		}
	    }
      }
    else      
      out.room[e] = st.room[e]; 
}

/***************************************************************************
 * ME_ Neighborhood Explorer stuff
 ***************************************************************************/

int ME_DeltaStudentClash::ComputeDeltaCost(const TT_State& st, const MoveEvent& mv) const
{
  int delta = 0;
  if (mv.old_slot != in.UNSCHEDULED)
    delta -= st.StudentClashCost(mv.event,mv.old_slot);
  if (mv.new_slot != in.UNSCHEDULED)
    delta += st.StudentClashCost(mv.event,mv.new_slot);
  return delta;
}

int ME_DeltaStudentClash2::ComputeDeltaCost(const TT_State& st, const MoveEvent& mv) const
{
  int delta = 0;
  if (mv.old_slot != in.UNSCHEDULED)
    delta -= st.StudentClashCost2(mv.event,mv.old_slot);
  if (mv.new_slot != in.UNSCHEDULED)
    delta += st.StudentClashCost2(mv.event,mv.new_slot);
  return delta;
}

int ME_DeltaEventPrecedence::ComputeDeltaCost(const TT_State& st, const MoveEvent& mv) const
{
  int delta = 0;
  if (mv.old_slot != in.UNSCHEDULED)
    delta -= st.EventPrecedenceCost(mv.event,mv.old_slot);
  if (mv.new_slot != in.UNSCHEDULED)
    delta += st.EventPrecedenceCost(mv.event,mv.new_slot);
  return delta;
}

int ME_DeltaUnscheduledEvents::ComputeDeltaCost(const TT_State& st, const MoveEvent& mv) const
{
  //  int delta = 0;
  if (mv.old_slot == in.UNSCHEDULED)
    return -in.EventStudentNumber(mv.event);
  else if (mv.new_slot == in.UNSCHEDULED)
    return in.EventStudentNumber(mv.event);
  else
    return 0;
  //  return delta;
}

int ME_DeltaUnscheduledEvents2::ComputeDeltaCost(const TT_State& st, const MoveEvent& mv) const
{
  //  int delta = 0;
  if (mv.old_slot == in.UNSCHEDULED)
    return -1;
  else if (mv.new_slot == in.UNSCHEDULED)
    return 1;
  else
    return 0;
  //  return delta;
}

int ME_DeltaStudentLastSlot::ComputeDeltaCost(const TT_State& st, const MoveEvent& mv) const
{
  int delta = 0;
  if (mv.old_slot % in.DAY_SLOTS == in.DAY_SLOTS - 1)
    delta -= in.EventStudentNumber(mv.event);
  if (mv.new_slot % in.DAY_SLOTS == in.DAY_SLOTS - 1)
    delta += in.EventStudentNumber(mv.event);
  return delta;

}

int ME_DeltaStudentClassRow::ComputeDeltaCost(const TT_State& st, const MoveEvent& mv) const
{
  int delta = 0;

  if (mv.new_slot == in.UNSCHEDULED)
    delta -= st.RemoveEventTripletCost(mv.event, mv.old_slot);
  else if (mv.old_slot == in.UNSCHEDULED)
    delta += st.InsertEventTripletCost(mv.event, mv.new_slot);
  else if (mv.new_slot > mv.old_slot + 2 || mv.old_slot > mv.new_slot + 2)
    {  // if slots are not close to each other: |old_slot - new_slot| >= 3
      delta -= st.RemoveEventTripletCost(mv.event, mv.old_slot);
      delta += st.InsertEventTripletCost(mv.event, mv.new_slot);
    }
  else
    { // in this case, we must consider the fact that the event is removed from mv.old_slot when 
      // computing the cost of adding it to mv.new_slot. To simplify the computation, we 
      // simulate the move before computing the added cost, and then retract it
      TT_State & st1 = (TT_State & ) st; // force constness of st
      delta -= st.RemoveEventTripletCost(mv.event, mv.old_slot);
      st1.DecreaseStudentTripletEvents(mv.event, mv.old_slot);
      delta += st.InsertEventTripletCost(mv.event, mv.new_slot);
      st1.IncreaseStudentTripletEvents(mv.event, mv.old_slot);
    }
  return delta;
}

int ME_DeltaStudentOneClassPerDay::ComputeDeltaCost(const TT_State& st, const MoveEvent& mv) const
{
  int delta = 0;  
  unsigned old_day = mv.old_slot/in.DAY_SLOTS;
  unsigned new_day = mv.new_slot/in.DAY_SLOTS;

  if (old_day != new_day)
    for (unsigned a = 0; a < in.EventStudentNumber(mv.event); a++)
      {
	unsigned s = in.EventStudentList(mv.event,a);
	if (mv.old_slot != in.UNSCHEDULED)
	  {
	    if (st.student_daily_events[s][old_day] == 2)
	      delta++;
	    if (st.student_daily_events[s][old_day] == 1)
	      delta--;
	  }
	if (mv.new_slot != in.UNSCHEDULED)
	  {
	    if (st.student_daily_events[s][new_day] == 1)
	      delta--;
	    if (st.student_daily_events[s][new_day] == 0)
	      delta++;
	  }
      }
  return delta;
}

MEE_NeighborhoodExplorer::MEE_NeighborhoodExplorer(const TT_Input& pin, StateManager<TT_Input,TT_State>& psm)
  : NeighborhoodExplorer<TT_Input,TT_State,MoveEvent>(pin, psm, "MEE_NeighborhoodExplorer") {} 

void MEE_NeighborhoodExplorer::AnyRandomMove(const TT_State& st, MoveEvent& mv) const
{
  do
    {
      mv.event = Random::Int(0,in.Events()-1);
      mv.old_slot = st.timeslot[mv.event];
      mv.old_room = st.room[mv.event];
      mv.new_slot = Random::Int(0,in.TIMESLOTS); // -1 removed
    }
  while (!FeasibleNewSlot(st,mv)); // FeasibleNewSlot also selects and assigns the appropriate new room
}

void MEE_NeighborhoodExplorer::RandomMove(const TT_State& st, MoveEvent& mv) const
{
  do
    {
      AnyRandomMove(st,mv);
    }
  while (!FeasibleMove(st,mv));
}


bool MEE_NeighborhoodExplorer::AnyNextMove(const TT_State& st, MoveEvent& mv) const
{
  if (!NextNewSlot(st,mv))
    {
      if (!NextEvent(st,mv))
	return false;   
    }  
  return true;
}

bool MEE_NeighborhoodExplorer::NextMove(const TT_State& st, MoveEvent& mv) const
{
  do
    {
      if (!AnyNextMove(st,mv))
	return false;
   }
  while (!FeasibleMove(st,mv));
  return true;
} 

void MEE_NeighborhoodExplorer::FirstMove(const TT_State& st, MoveEvent& mv) const
{
  mv.event = 0;
  mv.old_slot = st.timeslot[mv.event];
  mv.old_room = st.room[mv.event];
  if (!FirstNewSlot(st,mv))
    do
      AnyNextMove(st,mv);
    while (!FeasibleMove(st,mv));
} 


bool MEE_NeighborhoodExplorer::NextEvent(const TT_State& st, MoveEvent& mv)  const
{
  do
    {
      mv.event++;
      if (mv.event == in.Events())
	return false;
      mv.old_slot = st.timeslot[mv.event];
      mv.old_room = st.room[mv.event];
    }
  while (!FirstNewSlot(st,mv));
  return true;
}

bool MEE_NeighborhoodExplorer::FeasibleNewSlot(const TT_State& st, MoveEvent& mv) const
{
  return (mv.new_slot != mv.old_slot 	  
	  && (mv.new_slot == in.UNSCHEDULED || (in.EventPeriodAvailability(mv.event,mv.new_slot) 
					      && st.all_slot_events[mv.new_slot].size() < in.Rooms()))
	  && FindRoom(st,mv));
}


bool MEE_NeighborhoodExplorer::NextNewSlot(const TT_State& st, MoveEvent& mv)  const
{
  do
    { 
      mv.new_slot++;
      if (mv.new_slot == in.TIMESLOTS + 1)
	return false;
    }
  while (!FeasibleNewSlot(st,mv));
  return true;
}

bool MEE_NeighborhoodExplorer::FirstNewSlot(const TT_State& st, MoveEvent& mv)  const
{
  mv.new_slot = 0;
  while (!FeasibleNewSlot(st,mv))
    {
      mv.new_slot++; 
      if (mv.new_slot == in.TIMESLOTS + 1) // +1 added for unscheduled events
	return false;
    }
  return true;
}

bool MEE_NeighborhoodExplorer::FindRoom(const TT_State& st, MoveEvent& mv) const
{
  if (in.AnyRoomEvent(mv.event) || mv.new_slot == in.UNSCHEDULED)
    {
      mv.new_room = in.Rooms();
      mv.room_index = in.EventRoomNumber(mv.event);
      return true;
    }
  else
    {
      mv.room_index = st.FindFreeRoom(mv.event,mv.new_slot);
      if (mv.room_index != in.DUMMY_ROOM)
	{
	  mv.new_room = in.EventRoomList(mv.event,mv.room_index);
	  return true;
	}
      else
	return false;
    }
}

bool MEE_NeighborhoodExplorer::FeasibleMove(const TT_State& st, const MoveEvent& mv) const
{
  return true; // for efficiency because at present there are no infeasible moves generated
  // the old code was:
  //   return (mv.new_slot != mv.old_slot) 
  //     && (mv.new_slot == in.TIMESLOTS || 
  // 	(mv.new_room == in.Rooms() || st.SlotRoomFree(mv.new_slot,mv.new_room) 
  // 	 && in.EventPeriodAvailability(mv.event,mv.new_slot)));
} 

void MEE_NeighborhoodExplorer::MakeMove(TT_State& st, const MoveEvent& mv) const
{
  unsigned old_day = mv.old_slot/in.DAY_SLOTS, new_day = mv.new_slot/in.DAY_SLOTS;

  // update the core data
  st.timeslot[mv.event] = mv.new_slot;
  st.room[mv.event] = mv.new_room;

  // update redundant data: slot_events and slot_room_events
  if (mv.old_slot != in.UNSCHEDULED)
    {
      st.RemoveSlotEvent(mv.old_slot,mv.event);
      st.slot_room_event[mv.old_slot][mv.old_room] = in.NO_EVENT;
    }
  if (mv.new_slot != in.UNSCHEDULED)
    {
      st.InsertSlotEvent(mv.new_slot,mv.event);
      st.slot_room_event[mv.new_slot][mv.new_room] = mv.event;
    }

  // update redundant data: student_daily_events
  if (new_day == in.DAYS) // new_day is non-existing --> old_day must be existing
    st.DecreaseStudentDailyEvents(mv.event,old_day);
  else if (old_day == in.DAYS) 
    st.IncreaseStudentDailyEvents(mv.event,new_day);
  else if (old_day != new_day)
    {
      st.IncreaseStudentDailyEvents(mv.event,new_day);
      st.DecreaseStudentDailyEvents(mv.event,old_day);
    }

  // update redundant data: student_triplet_events
  if (mv.old_slot != in.UNSCHEDULED)  
    st.DecreaseStudentTripletEvents(mv.event,mv.old_slot);
  if (mv.new_slot != in.UNSCHEDULED)  
    st.IncreaseStudentTripletEvents(mv.event,mv.new_slot);
}

bool ME_NeighborhoodExplorer::FeasibleMove(const TT_State& st, const MoveEvent& mv) const
{
  return mv.new_slot != in.UNSCHEDULED;
} 

void ME_NeighborhoodExplorer::RandomMove(const TT_State& st, MoveEvent& mv) const
{
  do
    {
      mv.event = Random::Int(0,in.Events()-1);
      mv.old_slot = st.timeslot[mv.event];
      mv.old_room = st.room[mv.event];
      mv.new_slot = Random::Int(0,in.TIMESLOTS-1);
    }
  while (!FeasibleNewSlot(st,mv)); // FeasibleNewSlot also selects and assigns the appropriate new room
}


/***************************************************************************
 * SwapTime Neighborhood Explorer and related stuff
 ***************************************************************************/

int SwapTimeDeltaStudentClash::ComputeDeltaCost(const TT_State& st, const SwapTime& mv) const
{
  int delta = 0;

  if (mv.slot1 != in.UNSCHEDULED)
    {
      for (TT_State::EventList::const_iterator a = st.all_slot_events[mv.slot1].begin(); a != st.all_slot_events[mv.slot1].end(); a++)
	{
	  if (*a != mv.event1)
	    {
	      if (in.EventConflict(mv.event1,*a) > 0)	    
		delta -= min(in.EventStudentNumber(mv.event1),in.EventStudentNumber(*a));
	      if (in.EventConflict(mv.event2,*a) > 0)	    
		delta += min(in.EventStudentNumber(mv.event2),in.EventStudentNumber(*a));
	    }
	}
    }
  if (mv.slot2 != in.UNSCHEDULED)
    {
      for (TT_State::EventList::const_iterator a = st.all_slot_events[mv.slot2].begin(); a != st.all_slot_events[mv.slot2].end(); a++)
	{
	  if (*a != mv.event2)
	    {
	      if (in.EventConflict(mv.event1,*a) > 0)	    
		delta += min(in.EventStudentNumber(mv.event1),in.EventStudentNumber(*a));
	      if (in.EventConflict(mv.event2,*a) > 0)	    
		delta -= min(in.EventStudentNumber(mv.event2),in.EventStudentNumber(*a));
	    }
	}
    }
  return delta;
}

int SwapTimeDeltaStudentClash2::ComputeDeltaCost(const TT_State& st, const SwapTime& mv) const
{
  int delta = 0;

  if (mv.slot1 != in.UNSCHEDULED)
    {
      for (TT_State::EventList::const_iterator a = st.all_slot_events[mv.slot1].begin(); a != st.all_slot_events[mv.slot1].end(); a++)
	{
	  if (*a != mv.event1)
	    {
	      if (in.EventConflict(mv.event1,*a) > 0)	    
		delta--;
	      if (in.EventConflict(mv.event2,*a) > 0)	    
		delta++;
	    }
	}
    }
  if (mv.slot2 != in.UNSCHEDULED)
    {
      for (TT_State::EventList::const_iterator a = st.all_slot_events[mv.slot2].begin(); a != st.all_slot_events[mv.slot2].end(); a++)
	{
	  if (*a != mv.event2)
	    {
	      if (in.EventConflict(mv.event1,*a) > 0)	    
		delta++;
	      if (in.EventConflict(mv.event2,*a) > 0)	    
		delta--;
	    }
	}
    }
  return delta;
}

int SwapTimeDeltaEventPrecedence::ComputeDeltaCost(const TT_State& st, const SwapTime& mv) const
{
  int delta = 0;

  if (mv.slot1 != in.UNSCHEDULED)
    {
      delta -= st.EventPrecedenceCost(mv.event1,mv.slot1);
      delta += st.EventPrecedenceCost(mv.event2,mv.slot1);
    }
  if (mv.slot2 != in.UNSCHEDULED)
    {
      delta -= st.EventPrecedenceCost(mv.event2,mv.slot2);
      delta += st.EventPrecedenceCost(mv.event1,mv.slot2);
    }

  if (//mv.slot1 != in.UNSCHEDULED && mv.slot2 != in.UNSCHEDULED &&
      (in.EventPrecedence(mv.event1,mv.event2) || in.EventPrecedence(mv.event2,mv.event1))
      )
    // In this case the previous result must be corrected be the quantity below
    delta -= min(in.EventStudentNumber(mv.event1), in.EventStudentNumber(mv.event2));

  return delta;
}

int SwapTimeDeltaUnscheduledEvents::ComputeDeltaCost(const TT_State& st, const SwapTime& mv) const
{
  if (mv.slot1 == in.UNSCHEDULED)
    return in.EventStudentNumber(mv.event2) - in.EventStudentNumber(mv.event1);
  else if (mv.slot2 == in.UNSCHEDULED)
    return  in.EventStudentNumber(mv.event1) - in.EventStudentNumber(mv.event2);
  else
    return 0;
}

int SwapTimeDeltaStudentLastSlot::ComputeDeltaCost(const TT_State& st, const SwapTime& mv) const
{
  int delta = 0;
  if (mv.slot1 % in.DAY_SLOTS == in.DAY_SLOTS - 1)
    {
      delta -= in.EventStudentNumber(mv.event1);
      delta += in.EventStudentNumber(mv.event2);
    }
  if (mv.slot2 % in.DAY_SLOTS == in.DAY_SLOTS - 1)
    {
      delta -= in.EventStudentNumber(mv.event2);
      delta += in.EventStudentNumber(mv.event1);
    }
  return delta;
}

int SwapTimeDeltaStudentClassRow::ComputeDeltaCost(const TT_State& st, const SwapTime& mv) const
{
  int delta = 0;
  unsigned position1 = mv.slot1 % in.DAY_SLOTS;
  unsigned position2 = mv.slot2 % in.DAY_SLOTS;
  
  unsigned a, s;

  if (mv.slot1 == in.UNSCHEDULED)
    {
      delta += st.ReplaceEventTripletCost(mv.event2, mv.event1, mv.slot2);
    }
  else if (mv.slot2 == in.UNSCHEDULED)
    {
      delta += st.ReplaceEventTripletCost(mv.event1, mv.event2, mv.slot1);
    }

  // if slots are not close to each other: |slot1 - slot2| >= 3
  else if (mv.slot2 > mv.slot1 + 2 || mv.slot1 > mv.slot2 + 2)
    {
      delta += st.ReplaceEventTripletCost(mv.event1, mv.event2, mv.slot1);
      delta += st.ReplaceEventTripletCost(mv.event2, mv.event1, mv.slot2);
    }
  else
    { // Note: Could be improved using st.ReplaceEventTripletCost and related functions

      // in this case, we must consider the fact that the event is removed from mv.slot1 when 
      // computing the cost of adding it to mv.slot2, and vice versa. To simplify the computation, we 
      // simulate the move before computing the added cost, and then retract it
      
      TT_State & st1 = (TT_State & ) st; // force the  "constness" of st

      for (a = 0; a < in.EventStudentNumber(mv.event1); a++)
	{
	  s = in.EventStudentList(mv.event1,a);
	  if (!in.StudentEventEnrolment(s,mv.event2))
	    {
	      if (position1 > 1 
		  && st.student_triplet_events[s][mv.slot1-1] >= 3)
		delta--;	      
	      if (position1 > 0 && position1 < in.DAY_SLOTS - 1 
		  && st.student_triplet_events[s][mv.slot1] >= 3)
		delta--;
	      if (position1 < in.DAY_SLOTS - 2 
		  && st.student_triplet_events[s][mv.slot1+1] >= 3)
		delta--;
	      
	      if (mv.slot1 > 0)
		st1.student_triplet_events[s][mv.slot1-1]--;
	      st1.student_triplet_events[s][mv.slot1]--;
	      if (mv.slot1 < in.TIMESLOTS-1)
		st1.student_triplet_events[s][mv.slot1+1]--;
	      
	      if (position2 > 1 
		  && st.student_triplet_events[s][mv.slot2-1] >= 2)
		delta++;
	      if (position2 > 0 && position2 < in.DAY_SLOTS - 1 
		  && st.student_triplet_events[s][mv.slot2] >= 2)
		delta++;
	      if (position2 < in.DAY_SLOTS - 2 
		  && st.student_triplet_events[s][mv.slot2+1] >= 2)
		delta++;
	      
	      
	      if (mv.slot1 > 0)
		st1.student_triplet_events[s][mv.slot1-1]++;
	      st1.student_triplet_events[s][mv.slot1]++;
	      if (mv.slot1 < in.TIMESLOTS-1)
		st1.student_triplet_events[s][mv.slot1+1]++;
	    }
	}

      for (a = 0; a < in.EventStudentNumber(mv.event2); a++)
	{
	  s = in.EventStudentList(mv.event2,a);
	  if (!in.StudentEventEnrolment(s,mv.event1))
	    {
	      if (position2 > 1 
		  && st.student_triplet_events[s][mv.slot2-1] >= 3)
		delta--;	      
	      if (position2 > 0 && position2 < in.DAY_SLOTS - 1 
		  && st.student_triplet_events[s][mv.slot2] >= 3)
		delta--;
	      if (position2 < in.DAY_SLOTS - 2 
		  && st.student_triplet_events[s][mv.slot2+1] >= 3)
		delta--;
	      

	      if (mv.slot2 > 0)
		st1.student_triplet_events[s][mv.slot2-1]--;
	      st1.student_triplet_events[s][mv.slot2]--;
	      if (mv.slot2 < in.TIMESLOTS)
		st1.student_triplet_events[s][mv.slot2+1]--;
	      
	      if (position1 > 1 
		  && st.student_triplet_events[s][mv.slot1-1] >= 2)
		delta++;
	      if (position1 > 0 && position1 < in.DAY_SLOTS - 1 
		  && st.student_triplet_events[s][mv.slot1] >= 2)
		delta++;
	      if (position1 < in.DAY_SLOTS - 2 
		  && st.student_triplet_events[s][mv.slot1+1] >= 2)
		delta++;
	      
	      
	      if (mv.slot2 > 0)
		st1.student_triplet_events[s][mv.slot2-1]++;
	      st1.student_triplet_events[s][mv.slot2]++;
	      if (mv.slot2 < in.TIMESLOTS)
		st1.student_triplet_events[s][mv.slot2+1]++;
	    }
	}
    }
  return delta;
}

int SwapTimeDeltaStudentOneClassPerDay::ComputeDeltaCost(const TT_State& st, const SwapTime& mv) const
{  
  int delta = 0;
  unsigned a, s;  
  unsigned day1 = mv.slot1/in.DAY_SLOTS;
  unsigned day2 = mv.slot2/in.DAY_SLOTS;

  if (mv.slot1 == in.UNSCHEDULED)
    {
     for (a = 0; a < in.EventStudentNumber(mv.event1); a++)
	{
	  s = in.EventStudentList(mv.event1,a);
	  if (!in.StudentEventEnrolment(s,mv.event2))
	    {
	      if (st.student_daily_events[s][day2] == 1)
		delta--;
	      if (st.student_daily_events[s][day2] == 0)
		delta++;
	    }
	}
     for (a = 0; a < in.EventStudentNumber(mv.event2); a++)
       {
	 s = in.EventStudentList(mv.event2,a);
	 if (!in.StudentEventEnrolment(s,mv.event1))
	   {
	     if (st.student_daily_events[s][day2] == 2)
	       delta++;
	     if (st.student_daily_events[s][day2] == 1)
	       delta--;
	   }
       }

    }
  else if (mv.slot2 == in.UNSCHEDULED) 
    {
      for (a = 0; a < in.EventStudentNumber(mv.event1); a++)
	{
	  s = in.EventStudentList(mv.event1,a);
	  if (!in.StudentEventEnrolment(s,mv.event2))
	    {
	      if (st.student_daily_events[s][day1] == 2)
		delta++;
	      if (st.student_daily_events[s][day1] == 1)
		delta--;
	    }
	}
      for (a = 0; a < in.EventStudentNumber(mv.event2); a++)
	{
	  s = in.EventStudentList(mv.event2,a);
	  if (!in.StudentEventEnrolment(s,mv.event1))
	    {
	      if (st.student_daily_events[s][day1] == 1)
		delta--;
	      if (st.student_daily_events[s][day1] == 0)
		delta++;
	    }
	}
    }
  else
    {
      if (day1 != day2)
	{
	  for (a = 0; a < in.EventStudentNumber(mv.event1); a++)
	    {
	      s = in.EventStudentList(mv.event1,a);
	      if (!in.StudentEventEnrolment(s,mv.event2))
		{
		  if (st.student_daily_events[s][day1] == 2)
		    delta++;
		  if (st.student_daily_events[s][day1] == 1)
		    delta--;
		  if (st.student_daily_events[s][day2] == 1)
		    delta--;
		  if (st.student_daily_events[s][day2] == 0)
		    delta++;
		}
	    }
	  for (a = 0; a < in.EventStudentNumber(mv.event2); a++)
	    {
	      s = in.EventStudentList(mv.event2,a);
	      if (!in.StudentEventEnrolment(s,mv.event1))
		{
		  if (st.student_daily_events[s][day2] == 2)
		    delta++;
		  if (st.student_daily_events[s][day2] == 1)
		    delta--;
		  if (st.student_daily_events[s][day1] == 1)
		    delta--;
		  if (st.student_daily_events[s][day1] == 0)
		    delta++;
		}
	    }
	}
    }
  return delta;
}

SwapTime_NeighborhoodExplorer::SwapTime_NeighborhoodExplorer(const TT_Input& pin, StateManager<TT_Input,TT_State>& psm)   
  : NeighborhoodExplorer<TT_Input,TT_State,SwapTime>(pin, psm, "SwapTime_NeighborhoodExplorer") {} 

void SwapTime_NeighborhoodExplorer::RandomMove(const TT_State& st, SwapTime& mv) const
{
  do 
    {
      mv.event1 = Random::Int(0,in.Events()-1);
      mv.event2 = Random::Int(0,in.Events()-1);
      if (mv.event2 < mv.event1)
	swap(mv.event1, mv.event2);
      mv.slot1 = st.timeslot[mv.event1];
      mv.slot2 = st.timeslot[mv.event2];
      mv.old_room1 = st.room[mv.event1];
      mv.old_room2 = st.room[mv.event2];
      mv.new_room1 = st.FindFreeRoom(mv.event1,mv.slot2,mv.old_room2);
      mv.new_room2 = st.FindFreeRoom(mv.event2,mv.slot1,mv.old_room1);
    }
  while (!FeasibleMove(st,mv));
}

bool SwapTime_NeighborhoodExplorer::FeasibleMove(const TT_State& st, const SwapTime& mv) const
{ 
  return 
    mv.slot1 != mv.slot2  // includes mv.event1 != mv.event2 and both UNSCHEDULED
    && mv.new_room1 != in.DUMMY_ROOM
    && mv.new_room2 != in.DUMMY_ROOM
    && (mv.slot1 == in.UNSCHEDULED || in.EventPeriodAvailability(mv.event2,mv.slot1))
    && (mv.slot2 == in.UNSCHEDULED || in.EventPeriodAvailability(mv.event1,mv.slot2));
}

void SwapTime_NeighborhoodExplorerS::RandomMove(const TT_State& st, SwapTime& mv) const
{
//   unsigned i, j;
//   j = 0;
  do 
    {
//       i = 0;
      do
	{
	  mv.event1 = Random::Int(0,in.Events()-1);
	  mv.event2 = Random::Int(0,in.Events()-1);
	  if (mv.event2 < mv.event1)
	    swap(mv.event1, mv.event2);
	  mv.slot1 = st.timeslot[mv.event1];
	  mv.slot2 = st.timeslot[mv.event2];
	  mv.old_room1 = st.room[mv.event1];
	  mv.old_room2 = st.room[mv.event2];
// 	  i++;
	}      
      while (!FeasibleMoveSlots(st,mv));
//       cerr << i << " ";
      mv.new_room1 = st.FindFreeRoom(mv.event1,mv.slot2,mv.old_room2);
      mv.new_room2 = st.FindFreeRoom(mv.event2,mv.slot1,mv.old_room1);
//       j++;
    }
  while (mv.new_room1 == in.DUMMY_ROOM || mv.new_room2 == in.DUMMY_ROOM);
//   cerr << "(" << j << ") ";
}

bool SwapTime_NeighborhoodExplorerS::FeasibleMoveSlots(const TT_State& st, const SwapTime& mv) const
{ 
  return 
    mv.slot1 != mv.slot2  // includes mv.event1 != mv.event2
    && st.EventPrecedenceCost(mv.event1,mv.slot2) == 0
    && st.EventPrecedenceCost(mv.event2,mv.slot1) == 0
    && st.StudentClashCost(mv.event1,mv.slot2, mv.event2) == 0 // the cost is only the one due to e1 and e2
    && st.StudentClashCost(mv.event2,mv.slot1, mv.event1) == 0
    && in.EventPeriodAvailability(mv.event1,mv.slot2)
    && in.EventPeriodAvailability(mv.event2,mv.slot1);
}

bool SwapTime_NeighborhoodExplorerS::FeasibleMove(const TT_State& st, const SwapTime& mv) const
{ 
  return 
    mv.slot1 != mv.slot2  // includes mv.event1 != mv.event2
    && mv.new_room1 != in.DUMMY_ROOM
    && mv.new_room2 != in.DUMMY_ROOM
    && st.EventPrecedenceCost(mv.event1,mv.slot2) == 0
    && st.EventPrecedenceCost(mv.event2,mv.slot1) == 0
    && st.StudentClashCost(mv.event1,mv.slot2, mv.event2) == 0 // the cost is only the one due to e1 and e2
    && st.StudentClashCost(mv.event2,mv.slot1, mv.event1) == 0;
}

void SwapTime_NeighborhoodExplorer::MakeMove(TT_State& st, const SwapTime& mv) const
{
  unsigned day1 = mv.slot1/in.DAY_SLOTS, day2 = mv.slot2/in.DAY_SLOTS;

  // update the core data
  st.timeslot[mv.event1] = mv.slot2;
  st.timeslot[mv.event2] = mv.slot1;
  st.room[mv.event1] = mv.new_room1;
  st.room[mv.event2] = mv.new_room2;

  // update redundant data: all_slot_events and slot_room_events
  if (mv.slot1 != in.UNSCHEDULED)
    {
      st.ReplaceSlotEvent(mv.slot1, mv.event1, mv.event2);
      st.slot_room_event[mv.slot1][mv.old_room1] = in.NO_EVENT;
      st.slot_room_event[mv.slot1][mv.new_room2] = mv.event2;
    }
  if (mv.slot2 != in.UNSCHEDULED)
    {
      st.ReplaceSlotEvent(mv.slot2, mv.event2, mv.event1);  
      st.slot_room_event[mv.slot2][mv.old_room2] = in.NO_EVENT;
      st.slot_room_event[mv.slot2][mv.new_room1] = mv.event1;
    }

  // update redundant data: student_daily_events
  if (mv.slot1 == in.UNSCHEDULED)
    {
      st.IncreaseStudentDailyEvents(mv.event1,day2);
      st.DecreaseStudentDailyEvents(mv.event2,day2);
    }
  else if (mv.slot2 == in.UNSCHEDULED)
    {
      st.IncreaseStudentDailyEvents(mv.event2,day1);
      st.DecreaseStudentDailyEvents(mv.event1,day1);
    }
  else if (day1 != day2)
    {
      st.TransferStudentDailyEvents(mv.event1,day1,day2);
      st.TransferStudentDailyEvents(mv.event2,day2,day1);
    } 

   // update redundant data: student_triplet_events     
  if (mv.slot1 == in.UNSCHEDULED)
    {
      st.IncreaseStudentTripletEvents(mv.event1,mv.slot2);
      st.DecreaseStudentTripletEvents(mv.event2,mv.slot2);
    }
  else if (mv.slot2 == in.UNSCHEDULED)
    {
      st.DecreaseStudentTripletEvents(mv.event1,mv.slot1);
      st.IncreaseStudentTripletEvents(mv.event2,mv.slot1);
    }
  else
    {
      st.DecreaseStudentTripletEvents(mv.event1,mv.slot1);
      st.IncreaseStudentTripletEvents(mv.event1,mv.slot2);
      st.DecreaseStudentTripletEvents(mv.event2,mv.slot2);
      st.IncreaseStudentTripletEvents(mv.event2,mv.slot1);
    }
}


bool SwapTime_NeighborhoodExplorer::NextMove(const TT_State& st, SwapTime& mv) const
{
  do
    {
      if (!AnyNextMove(st,mv))
	return false;
    }
  while (!FeasibleMove(st,mv));
  return true;
}


void SwapTime_NeighborhoodExplorer::FirstMove(const TT_State& st, SwapTime& mv) const
{
  mv.event1 = 0;
  mv.event2 = 1;
  mv.slot1 = st.timeslot[mv.event1];
  mv.slot2 = st.timeslot[mv.event2];
  mv.old_room1 = st.room[mv.event1];
  mv.old_room2 = st.room[mv.event2];
  mv.new_room1 = st.FindFreeRoom(mv.event1,mv.slot2,mv.old_room2);
  mv.new_room2 = st.FindFreeRoom(mv.event2,mv.slot1,mv.old_room1);

  while (!FeasibleMove(st,mv))
    AnyNextMove(st,mv);
}


bool SwapTime_NeighborhoodExplorer::AnyNextMove(const TT_State& st, SwapTime& mv)  const
{
  if (mv.event2 < in.Events()-1)
    mv.event2++;
  else if (mv.event1 < in.Events()-2)    
    {
      mv.event1++;
      mv.event2 = mv.event1 + 1;
    }
  else
    return false;
  mv.slot1 = st.timeslot[mv.event1];
  mv.slot2 = st.timeslot[mv.event2];
  mv.old_room1 = st.room[mv.event1];
  mv.old_room2 = st.room[mv.event2];
  mv.new_room1 = st.FindFreeRoom(mv.event1,mv.slot2,mv.old_room2);
  mv.new_room2 = st.FindFreeRoom(mv.event2,mv.slot1,mv.old_room1);
  return true;
}
