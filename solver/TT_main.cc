// File main.cpp

#include <iostream>
#include "TT_BasicClasses.hh"
#include "TT_Helpers.hh"
#include "TT_Runners.hh"
#include <Solvers.hh>
#include <Testers.hh>
#include <helpers/MultimodalNeighborhoodExplorer.hh>
#include <utils/CLParser.hh>

using namespace std;

int main(int argc, char* argv[])
{
  CLParser cl(argc, argv);
  ValArgument<string> arg_instance("instance", "i", true, cl);
  FlagArgument arg_tester("tester","t",cl);
  ValArgument<unsigned> arg_observer("observer","ob", false,cl);
  FlagArgument arg_no_pred("arg_no_pred", "np", cl);
  FlagArgument arg_hard_only("hard_only", "ho", cl);
  FlagArgument arg_hard_version_two("hard2", "h2", cl); // use version 2 of hard cost (all clashes cost = 1)
  FlagArgument arg_old_format("old_format", "of", cl);
  ValArgument<unsigned> arg_hard_weight("hard_weight", "hw", false, cl);
  ValArgument<unsigned> arg_seed("seed", "s", false, cl);
  ValArgument<string> arg_init_state("init_state", "is", false, cl);
  ValArgument<string> arg_method("method", "m", false, cl);
  ValArgument<string> arg_write_output_to_file("write_output_file", "wof", false, cl);
  ValArgument<double> arg_timeout("timeout", "to", false, cl);
  ValArgument<double> arg_swap_rate("swap_rate", "sr", false, cl);

  cl.MatchArgument(arg_instance);
  cl.MatchArgument(arg_no_pred);
  cl.MatchArgument(arg_hard_only);
  cl.MatchArgument(arg_hard_version_two);
  cl.MatchArgument(arg_hard_weight);
  cl.MatchArgument(arg_old_format);
  cl.MatchArgument(arg_seed);
  cl.MatchArgument(arg_swap_rate);
 
  if (arg_seed.IsSet())
    Random::Seed(arg_seed.GetValue());

  TT_Input in(arg_instance.GetValue(),arg_old_format.IsSet());
  TT_Output out(in);
  TT_State st(in);

  const int basic_hard_weight = 10000;
  int hard_weight;

  if (arg_hard_only.IsSet())
    hard_weight = 1;
  else if (arg_hard_weight.IsSet())
    hard_weight = arg_hard_weight.GetValue();
  else
    hard_weight = basic_hard_weight;

  // Helpers
  UnscheduledEvents cc0(in,hard_weight,true); 
  UnscheduledEvents2 cc0_2(in,hard_weight,true); 
  StudentClash cc1(in,hard_weight,true);
  StudentClash2 cc1_2(in,2*hard_weight,true);
  EventPrecedence cc3(in,2*hard_weight,true); 

  StudentLastSlot cc4(in,1,false);
  StudentClassRow cc5(in,1,false);
  StudentOneClassPerDay cc6(in,1,false);

  TT_StateManager sm(in), smAux(in); // smAux is needed to restore the basic hard weight

  if (arg_hard_version_two.IsSet())
    {
      sm.AddCostComponent(cc0_2);
      sm.AddCostComponent(cc1_2);      
    }
  else
    {
      sm.AddCostComponent(cc0);
      sm.AddCostComponent(cc1);
    }

  if (!arg_no_pred.IsSet())
    {
      sm.AddCostComponent(cc3);
    }      
  if (!arg_hard_only.IsSet())
    {
      sm.AddCostComponent(cc4);
      sm.AddCostComponent(cc5);
      sm.AddCostComponent(cc6);
    }

  TT_OutputManager om(in);

  ME_DeltaUnscheduledEvents me_cc0(in,cc0);
  ME_DeltaUnscheduledEvents2 me_cc0_2(in,cc0_2);
  ME_DeltaStudentClash me_cc1(in,cc1);
  ME_DeltaStudentClash2 me_cc1_2(in,cc1_2);
  ME_DeltaEventPrecedence me_cc3(in,cc3);
  ME_DeltaStudentLastSlot me_cc4(in,cc4);
  ME_DeltaStudentClassRow me_cc5(in,cc5);
  ME_DeltaStudentOneClassPerDay me_cc6(in,cc6);

  MEE_NeighborhoodExplorer mee_nhe(in,sm);
  ME_NeighborhoodExplorer me_nhe(in,sm);

  if (arg_hard_version_two.IsSet())
    {
      mee_nhe.AddDeltaCostComponent(me_cc0_2);
      mee_nhe.AddDeltaCostComponent(me_cc1_2);
      me_nhe.AddDeltaCostComponent(me_cc0_2);
      me_nhe.AddDeltaCostComponent(me_cc1_2);
    }
  else
    {
      mee_nhe.AddDeltaCostComponent(me_cc0);
      mee_nhe.AddDeltaCostComponent(me_cc1);
      me_nhe.AddDeltaCostComponent(me_cc0);
      me_nhe.AddDeltaCostComponent(me_cc1);
    }


  if (!arg_no_pred.IsSet())
    {
      mee_nhe.AddDeltaCostComponent(me_cc3);
      me_nhe.AddDeltaCostComponent(me_cc3);
    }
  if (!arg_hard_only.IsSet())
    {
      mee_nhe.AddDeltaCostComponent(me_cc4);
      mee_nhe.AddDeltaCostComponent(me_cc5);
      mee_nhe.AddDeltaCostComponent(me_cc6);
      me_nhe.AddDeltaCostComponent(me_cc4);
      me_nhe.AddDeltaCostComponent(me_cc5);
      me_nhe.AddDeltaCostComponent(me_cc6);
    }

  SwapTime_NeighborhoodExplorer st_nhe(in,sm);

  SwapTimeDeltaUnscheduledEvents st_cc0(in,cc0);
  SwapTimeDeltaStudentClash st_cc1(in,cc1);
  SwapTimeDeltaStudentClash2 st_cc1_2(in,cc1_2);
  SwapTimeDeltaEventPrecedence st_cc3(in,cc3);
  SwapTimeDeltaStudentLastSlot st_cc4(in,cc4);
  SwapTimeDeltaStudentClassRow st_cc5(in,cc5);
  SwapTimeDeltaStudentOneClassPerDay st_cc6(in,cc6);

  if (arg_hard_version_two.IsSet())
    {
      // no component st_cc0_2 is present
      st_nhe.AddDeltaCostComponent(st_cc1_2);
    }
  else
    {
      st_nhe.AddDeltaCostComponent(st_cc0);
      st_nhe.AddDeltaCostComponent(st_cc1);
    }

  if (!arg_no_pred.IsSet())
    {
      st_nhe.AddDeltaCostComponent(st_cc3);
    }
  if (!arg_hard_only.IsSet())
    {
      st_nhe.AddDeltaCostComponent(st_cc4);
      st_nhe.AddDeltaCostComponent(st_cc5);
      st_nhe.AddDeltaCostComponent(st_cc6);
    }

  vector<double> nhe_distribution(2,0.5); // (0.5,05) is the default distribution
  if (arg_swap_rate.IsSet())
    {
      nhe_distribution[0] = 1 - arg_swap_rate.GetValue();
      nhe_distribution[1] = arg_swap_rate.GetValue();
    }

  typedef PrepareSetUnionNeighborhoodExplorerTypes<TT_Input, TT_State, TYPELIST_2(ME_NeighborhoodExplorer, SwapTime_NeighborhoodExplorer)> BimodalType2;
  typedef BimodalType2::MoveList ME_ST_Move; 
  BimodalType2::NeighborhoodExplorer me_st_nhe(in, sm, nhe_distribution, "ME+ST");
  me_st_nhe.AddNeighborhoodExplorer(st_nhe);
  me_st_nhe.AddNeighborhoodExplorer(me_nhe);

  typedef PrepareSetUnionNeighborhoodExplorerTypes<TT_Input, TT_State, TYPELIST_2(MEE_NeighborhoodExplorer, SwapTime_NeighborhoodExplorer)> BimodalType3;
  typedef BimodalType3::MoveList MEE_ST_Move; 
  BimodalType3::NeighborhoodExplorer mee_st_nhe(in, sm, nhe_distribution, "MEE+ST");
  mee_st_nhe.AddNeighborhoodExplorer(st_nhe);
  mee_st_nhe.AddNeighborhoodExplorer(mee_nhe);

  SteepestDescent<TT_Input, TT_State,MoveEvent>  mee_sd(in,sm,mee_nhe,"MEE_SteepestDescend");
  HillClimbing<TT_Input, TT_State,MoveEvent>  mee_hc(in,sm,mee_nhe,"MEE_HillClimbing");
  SteepestDescent<TT_Input, TT_State,MoveEvent>  me_sd(in,sm,me_nhe,"ME_SteepestDescend");
  HillClimbing<TT_Input, TT_State,MoveEvent>  me_hc(in,sm,me_nhe,"ME_HillClimbing");
  SimulatedAnnealing<TT_Input, TT_State,ME_ST_Move>  me_st_sa(in,sm,me_st_nhe,"ME_ST_SA",cl);
  SimulatedAnnealing<TT_Input, TT_State,MEE_ST_Move>  mee_st_sa(in,sm,mee_st_nhe,"MEE_ST_SA",cl);
  HillClimbing<TT_Input, TT_State,MEE_ST_Move>  mee_st_hc(in,sm,mee_st_nhe,"MEE_ST_HC",cl);

  cl.MatchArguments();
  
  unsigned observe_level = 1;
  if (arg_observer.IsSet())
    observe_level = arg_observer.GetValue();
      
  RunnerObserver<TT_Input, TT_State, MoveEvent> me_rob(observe_level,0);
  RunnerObserver<TT_Input, TT_State, ME_ST_Move> me_st_rob(observe_level,0);
  RunnerObserver<TT_Input, TT_State, MEE_ST_Move> mee_st_rob(observe_level,0);

  if (arg_observer.IsSet())
   {
     me_sd.AttachObserver(me_rob);
     me_hc.AttachObserver(me_rob);
     me_sd.AttachObserver(me_rob);
     me_hc.AttachObserver(me_rob);
     me_st_sa.AttachObserver(me_st_rob);
     mee_st_sa.AttachObserver(mee_st_rob);
   }
 if (arg_tester.IsSet())
   {
     MoveTester<TT_Input, TT_Output, TT_State, MoveEvent> me_move_test(in, sm, om, mee_nhe, "MoveEventExtended");
     MoveTester<TT_Input, TT_Output, TT_State, MoveEvent> me_move_R_test(in, sm, om, me_nhe, "MoveEvent");
     MoveTester<TT_Input, TT_Output, TT_State, SwapTime> swap_move_test(in, sm, om, st_nhe, "SwapTime");
     MoveTester<TT_Input, TT_Output, TT_State, ME_ST_Move> bimodal_move_test(in, sm, om, me_st_nhe, "MoveEvent+SwapTime");
     Tester<TT_Input, TT_Output, TT_State> tester(in, sm, om);
     
     tester.AddMoveTester(me_move_test);
     tester.AddMoveTester(me_move_R_test);
     tester.AddMoveTester(swap_move_test);
     tester.AddMoveTester(bimodal_move_test);
     
     tester.AddRunner(mee_sd);
     tester.AddRunner(mee_hc);
     tester.AddRunner(me_sd);
     tester.AddRunner(me_hc);
     tester.AddRunner(me_st_sa);
     tester.AddRunner(mee_st_sa);

     if (arg_init_state.IsSet())
       tester.RunMainMenu(arg_init_state.GetValue());
     else
       tester.RunMainMenu();
   }
 else // No tester: bacth run
   {
     string method = arg_method.GetValue();
     GeneralizedLocalSearch<TT_Input,TT_Output,TT_State> solver(in,sm,om,"S");
     GeneralizedLocalSearchObserver<TT_Input, TT_Output, TT_State> ob(1);

     solver.SetIdleRounds(1);
     solver.SetRounds(1);    
     Chronometer chrono;
     chrono.Start();	
     if (arg_timeout.IsSet())
       {
	 solver.SetTimeout(arg_timeout.GetValue());
       }


     if (method == "ME_ST_SA_R") // starts from a state with no unscheduled  
       {
	 solver.AddRunner(me_st_sa);
	 solver.SimpleSolve(0,2); // runner 0, greedy state (the one without UNSCHEDULED)
       }
    else if (method == "ME_ST_SA")  
       {
	 solver.AddRunner(me_st_sa);
	 solver.SimpleSolve();
       }
     else if (method == "MEE_ST_SA")
       {
	 solver.AddRunner(mee_st_sa);
	 solver.SimpleSolve();
       }

     if (!arg_hard_only.IsSet() && sm.Violations(solver.GetBestState()) > 0)
       // run a post-processing HC for "converting" violations of type 1 and 3 into type 0,
       // removes also possible violations due to a low value of hard_weight
       {
	 cc0.SetWeight(basic_hard_weight);
	 cc1.SetWeight(2*basic_hard_weight);	 
	 cc3.SetWeight(2*basic_hard_weight);	 
	 cc0_2.SetWeight(basic_hard_weight);
	 cc1_2.SetWeight(2*basic_hard_weight);	 
	 
	 solver.ClearRunners();
	 solver.AddRunner(mee_st_hc);
	 mee_st_hc.SetMaxIdleIteration(1000000);
	 solver.SimpleSolve(0,0); // runner 0, from current state (0)
       }

     chrono.Stop();
     
     if (arg_write_output_to_file.IsSet())
       {
	 out = solver.GetOutput();
	 std::ofstream os(arg_write_output_to_file.GetValue().c_str());
	 os << out;
	 os << "Seed " << Random::GetSeed() << endl;
	 os << "Time " << chrono.TotalTime() << endl;
	 if (!arg_hard_only.IsSet())
	   {
	     cc0.SetWeight(basic_hard_weight);
	     cc1.SetWeight(basic_hard_weight);	 
	     cc3.SetWeight(basic_hard_weight);	 
	     cc0_2.SetWeight(basic_hard_weight);
	     cc1_2.SetWeight(basic_hard_weight);	 
	     os << "Cost " << sm.CostFunction(solver.GetBestState()) << endl;
	   }
	 else // hard only
	   {
	     if (!arg_hard_version_two.IsSet())
	       { // write the cost in version 2 (even if we used version 1 for running)
		 smAux.AddCostComponent(cc0_2);
		 smAux.AddCostComponent(cc1_2);
		 os << "Cost " << smAux.CostFunction(solver.GetBestState()) << endl;
	       }	   
	     else
	       os << "Cost " << solver.GetBestCost() << endl;
	   }
       }
     else
       {
	 out = solver.GetOutput();
	 cout << out << endl; 
	 cout << Random::GetSeed() << endl << chrono.TotalTime() << endl // << partial_time << " "
	      << method << endl << solver.GetBestCost() << endl;
       }
   }
  
  return 0;
}
