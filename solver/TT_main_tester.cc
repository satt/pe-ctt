// File TT_main_tester.cc

#include <iostream>
#include "TT_BasicClasses.hh"
#include "TT_Helpers.hh"
#include "TT_Runners.hh"
#include "Solvers.hh"
#include "extra/runners/BimodalHillClimbing.hh"
#include "Testers.hh"

#include "utils/clparser/CLParser.hh"
#include "utils/clparser/ValArgument.hh"
#include "utils/clparser/FlagArgument.hh"

using namespace std;

int main(int argc, char* argv[])
{

  CLParser cl(argc, argv);
#include "main_args_common.icc"  // Read main arg options
  cl.MatchArguments();

  if (arg_seed.IsSet())
    Random::Seed(arg_seed.GetValue());

  string file_name = arg_instance.GetValue(); 
 
  std::cout << "Load data of instance... " << file_name; 
  TT_Input in(file_name);
  std::cout << " (done)" << std::endl;

#include "objects.icc"

  MoveTester<TT_Input, TT_Output, TT_State, MoveEvent> both_move_test(in, sm, om, bnhe, "MoveEvent");
  MoveTester<TT_Input, TT_Output, TT_State, MoveEvent> both_move_RM_test(in, sm, om, bnherM, "MoveEventrM");
  MoveTester<TT_Input, TT_Output, TT_State, SwapTimeAndRoom> swap_move_test(in, sm, om, stnhe, "SwapTimeAndRoom");
  KickerTester<TT_Input, TT_Output, TT_State> k_test(in, sm, om, k, "Kicker");
  Tester<TT_Input, TT_Output, TT_State> tester(in, sm, om);

  tester.AddMoveTester(both_move_test);
  tester.AddMoveTester(both_move_RM_test);
  tester.AddMoveTester(swap_move_test);
  tester.AddKickerTester(k_test);

  tester.AddRunner(bts);
  tester.AddRunner(btswsp);
//  tester.AddRunner(bbshc);
  tester.AddRunner(bsdrm);
  tester.AddRunner(bhcrm);
  tester.AddRunner(bsdr);
  tester.AddRunner(bsd);
  tester.AddRunner(bfd);
  tester.AddRunner(btswsprM);


 if (arg_init_state.IsSet())
    tester.RunMainMenu(arg_init_state.GetValue());
  else
    tester.RunMainMenu();
  return 0;
}
