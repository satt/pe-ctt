// file BasicClasses.cpp

#include <cstdlib>
#include <cassert>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <map>
#include "TT_BasicClasses.hh"
#include "utils/Random.hh"
#include <sstream>

using namespace std;

TT_Input::TT_Input(string load_instance_name, bool old_format)
  : TIMESLOTS(45), DAYS(5), DAY_SLOTS(9), UNSCHEDULED(TIMESLOTS), NO_EVENT(-1)
{
  unsigned r, e, e1, e2, e3, f, p, s;
  unsigned a2, a3;
  unsigned features;

  ifstream is(load_instance_name.c_str());
  if (is.fail())
    throw std::logic_error("I couldn't open the file " + load_instance_name);

  is >> events >> rooms >> features >> students;
  ANY_ROOM = rooms;
  DUMMY_ROOM = rooms + 1;

  vector<unsigned> room_size(rooms);  
  vector<vector<bool> > room_feature(rooms,vector<bool>(features));
  vector<vector<bool> > event_feature(events,vector<bool>(features));

  // Used to convert precedences to unavailabilities
  vector<unsigned> min_period(events,0), max_period(events,TIMESLOTS-1); 

  student_event_enrolment.resize(students);
  for (s = 0; s < students; s++)
    student_event_enrolment[s].resize(events,0);
  event_student_list.resize(events,vector<unsigned>(0));
  event_room_list.resize(events,vector<unsigned>(0));
  event_event_precedence.resize(events,vector<bool>(events,false));
  event_period_availability.resize(events,vector<bool>(TIMESLOTS,true));
  anyroom_event.resize(events,false);
  event_before_list.resize(events); 
  event_after_list.resize(events);

  for (r = 0; r < rooms; r++)
    is >> room_size[r];

  int bool_val, int_val;
  for (s = 0; s < students; s++)
    for (e = 0; e < events; e++)
      {
	is >> bool_val;
	student_event_enrolment[s][e] = bool_val;
	if (student_event_enrolment[s][e]) 
	  event_student_list[e].push_back(s);
      }

  for (r = 0; r < rooms; r++)
    for (f = 0; f < features; f++)
      {
	is >> bool_val;
	room_feature[r][f] = bool_val;
      }

  for (e = 0; e < events; e++)
    for (f = 0; f < features; f++)
      {
	is >> bool_val;
	event_feature[e][f] = bool_val;
      }

  if (!old_format) // if old_format is true the file is over and all data structures remain with their default values
    {
      for (e = 0; e < events; e++)
	for (p = 0; p < TIMESLOTS; p++)
	  {
	    is >> bool_val;
	    event_period_availability[e][p] = bool_val;
	  }
      
      for (e1 = 0; e1 < events; e1++)
	for (e2 = 0; e2 < events; e2++)
	  {
	    is >> int_val;
	    if (int_val == 1)
	      {
		event_event_precedence[e1][e2] = true;
		event_before_list[e2].push_back(e1);
		event_after_list[e1].push_back(e2);
		if (min_period[e2] < min_period[e1] + 1)
		  min_period[e2] = min_period[e1] + 1;
		if (max_period[e1] > max_period[e2] - 1)
		  max_period[e1] = max_period[e2] - 1;
		
	      }
	  }
      
      for (e1 = 0; e1 < events; e1++)
	for (a2 = 0; a2 < event_after_list[e1].size(); a2++)
	  {
	    e2 = event_after_list[e1][a2];
	    for (a3 = 0; a3 < event_after_list[e2].size(); a3++)
	      {
		e3 = event_after_list[e2][a3];
		if (!event_event_precedence[e1][e3])
		  {
		    if (e1 == e3)
		      {
			cerr << "Error: precedence cycle on : " << e1 << endl;
			exit(1);
		      }
		    else
		      {
			// cerr << "Add " << e1 << "," << e3 << " because of " << e2 << endl;
			event_event_precedence[e1][e3] = true;
			event_before_list[e3].push_back(e1);
			event_after_list[e1].push_back(e3);
			if (min_period[e3] < min_period[e1] + 1)
			  min_period[e3] = min_period[e1] + 1;
			if (max_period[e1] > max_period[e3] - 1)
			  max_period[e1] = max_period[e3] - 1;
		      }
		  }
	      }
	  }
      for (e = 0; e < events; e++)
	{
	  for (p = 0; p < min_period[e]; p++)
	    event_period_availability[e][p] = false;
	  for (p = TIMESLOTS-1; p > max_period[e]; p--)
	    event_period_availability[e][p] = false;
	}
    }
  is.close();



  // *************************************************
  // compute the derived data from basic (input) ones
  // *************************************************

  // compute event_event_conflict
  // Note: the following loop assigns to event_event_conflict(e1,e2) the number of common students. 
  // event_event_conflict(e,e) gets value equal to the number of enrolment of an event e. 
  // We use such value for the function EventStudentNumber.

  event_event_conflict.resize(events, vector<unsigned>(events,0));
  for (e1 = 0; e1 < events; e1++)
    for (s = 0; s < students; s++)
      if (student_event_enrolment[s][e1])
	for (e2 = 0; e2 < events; e2++)
	  if (student_event_enrolment[s][e2])
	    event_event_conflict[e1][e2]++;

  // compute event_room_compatibility
  event_room_compatibility.resize(events,vector<bool>(rooms,true));
  for (e = 0; e < events; e++)
    {
      for (r = 0; r < rooms; r++)
	{
	  if (room_size[r] < event_event_conflict[e][e])
	    {
	      event_room_compatibility[e][r] = false;
	      continue;
	    }
	  for (f = 0; f < features; f++)
	    if (!room_feature[r][f] && event_feature[e][f])
	      {
		event_room_compatibility[e][r] = false;
		break;
	      }
 	  if (event_room_compatibility[e][r])
	    event_room_list[e].push_back(r);
	}
      if (event_room_list[e].size() == rooms)
	anyroom_event[e] = true;
    }

  // Order available rooms based on their weighted compatibility:
  // Most requested rooms are placed at the end so that they are 
  // selected only if no other is available (27-08-07)

  vector<pair<unsigned,double> > ordered_rooms;
  unsigned total_events, i;
  double weighted_events;
  for (r = 0; r < rooms; r++)
    {
      total_events = 0;
      weighted_events = 0.0;
      for (e = 0; e < events; e++)
	if (event_room_compatibility[e][r])
	  {
	    total_events++;
	    weighted_events += 1.0/event_room_list[e].size();
	  }
      for (i = 0; i < ordered_rooms.size(); i++)
	if (weighted_events < ordered_rooms[i].second)
	  {
	    ordered_rooms.insert(ordered_rooms.begin() + i, make_pair<unsigned,double>(r,weighted_events));
	    break;
	  }
      if (i == ordered_rooms.size())
	ordered_rooms.push_back(make_pair<unsigned,double>(r,weighted_events));

    }

  // remove it, so as to regenerate it in the correct order
  for (e = 0; e < events; e++)
    event_room_list[e].clear();
  
  for (i = 0; i < rooms; i++)
    {
      r = ordered_rooms[i].first;
      for (e = 0; e < events; e++)
	if (event_room_compatibility[e][r])
	  {
	    event_room_list[e].push_back(r);
	  }
    }
      
  // update event conflicts based on common single room compatibility
  // (29-9-2004, see Kostuch, Patat-04)

  for (e1 = 0; e1 < events; e1++)
    if (event_room_list[e1].size() == 1)
      for (e2 = 0; e2 < events; e2++)
	if (e2 != e1 && event_room_list[e2].size() == 1)
	  if (event_room_list[e1][0] == event_room_list[e2][0] &&
	      event_event_conflict[e1][e2] == 0)
	    event_event_conflict[e1][e2] = 1;

  unsigned weighted_sum = 0, sum = 0;
  for (e = 0; e < events; e++)
    {
      for (e1 = 0; e1 < events; e1++)
	if (event_event_conflict[e][e1] > 0 && e != e1)
	  {
	    sum++;
	    weighted_sum += event_event_conflict[e][e1];
	  }
    }

  weighted_density = 2.0*weighted_sum/(events * (events-1));
  weighted_conflicts = float(weighted_sum)/events;
  density = 2.0*sum/(events * (events-1));
  conflicts = float(sum)/events;
}

ostream& operator<<(ostream& os, const TT_Input& in)
{
  unsigned e, e1, s, sum, sum1, count, p, free_events; 
  float average_conflicts_per_event, average_students_per_event,
    average_events_per_student, average_rooms_per_event, conflicts_density;
  float average_weighted_conflicts_per_event, conflicts_weighted_density;

  sum = 0;
  os << "Student event enrolment" << endl;
  for (s = 0; s < in.students; s++)
    {      
      for (e = 0; e < in.events; e++)
	if (in.student_event_enrolment[s][e])
	  sum++;
    }
  average_events_per_student = float(sum)/in.students;

  sum = 0;
  os << "Event student list" << endl;
  for (e = 0; e < in.events; e++)
    {
      sum += in.event_student_list[e].size();
      os << e << "(" << in.event_event_conflict[e][e] << ") : ";
      for (unsigned a = 0; a < in.event_student_list[e].size(); a++)
	os << in.event_student_list[e][a] << ' ';
      os << endl;
    }
  average_students_per_event = float(sum)/in.events;
  os << endl;

  sum = 0;
  free_events = 0;
  os << "Event room list" << endl;
  for (e = 0; e < in.events; e++)
    {
      sum += in.event_room_list[e].size();
      os << e << " : ";
      for (unsigned a = 0; a < in.event_room_list[e].size(); a++)
	os << in.event_room_list[e][a] << ' ';
      os << endl;
      if (in.event_room_list[e].size() == in.rooms)
	free_events++;
    }
  os << endl;
  average_rooms_per_event = float(sum)/in.events;

  os << "Any room events" << endl;
  for (e = 0; e < in.events; e++)
    if (in.anyroom_event[e])
      os << e << ' ';
  os << endl;
  
  sum = 0;
  sum1 = 0;
  os << "Event-Event list" << endl;
  for (e = 0; e < in.events; e++)
    {
      count = 0;
      for (e1 = 0; e1 < in.events; e1++)
	if (in.event_event_conflict[e][e1] > 0 && e != e1)
	  {
	    count++;
	    sum1 += in.event_event_conflict[e][e1];
	  }
      sum += count;

      os << e << "(" << count << ") : ";
      for (e1 = 0; e1 < in.events; e1++)
	if (in.event_event_conflict[e][e1] > 0 && e != e1)
	  os << e1 << ' ';// << "(" << in.event_event_conflict[e][e1] << ") ";
      os << endl;
    }

  os << endl;
  os << "Event period list" << endl;
  for (e = 0; e < in.events; e++)
    {
      os << e << " : ";
      for (p = 0; p < in.TIMESLOTS; p++)
	if (in.event_period_availability[e][p])
	  os << p << ' ';
      os << endl;
    }
  os << endl;

  os << "Event event precedence " << endl;
  for (e1 = 0; e1 < in.events; e1++)
    {
      unsigned a;
      os << e1 << "(" << in.event_before_list[e1].size() << "," << in.event_after_list[e1].size() << ") : ";
      for (a = 0; a < in.event_before_list[e1].size(); a++)
	os <<  in.event_before_list[e1][a] << ' ';
      os << " || ";
      for (a = 0; a < in.event_after_list[e1].size(); a++)
	os <<  in.event_after_list[e1][a] << ' ';
      os << endl;
    }
  os << endl;


  average_weighted_conflicts_per_event = float(sum1)/in.events;
  average_conflicts_per_event = float(sum)/in.events;
  conflicts_density = float(sum)/(in.events * (in.events-1));
  conflicts_weighted_density = float(sum1)/(in.events * (in.events-1));

  os << endl;

  //   os << "Event-Event conflict" << endl;
  //   for (e = 0; e < in.events; e++)
  //     {
  //       for (e1 = 0; e1 < in.events; e1++)
  // 	os << in.event_event_conflict[e][e1] << ' ';
  //       os << endl;
  //     }
  //   os << endl;

  os << "Students per event : " << endl;
  for (s = 0; s < in.students; s++)
    {
      count = 0;
      for (e = 0; e < in.events; e++)
	{
	  if (in.event_student_list[e].size() == unsigned(s))
	    count++;
	}
      if (count > 0)
	os << "Number of events with " << s << " students : " << count << endl;
    }
  os << endl;

  os << "Events: " << in.events << ", Rooms: "  << in.rooms << ", Students: " << in.students << endl;
  os << "Room occupancy: " << in.events * 100.0 / (in.rooms * in.TIMESLOTS) << '%' << endl;
  os << "Average conflicts per event : " << average_conflicts_per_event << endl;
  os << "Conflicts density : " << conflicts_density << endl;
  os << "Average weighted conflicts per event : " << average_weighted_conflicts_per_event << endl;
  os << "Conflicts weighted density : " << conflicts_weighted_density << endl;
  os << "Average students per event : " << average_students_per_event << endl;
  os << "Average events per student : " << average_events_per_student << endl;
  os << "Average rooms per event: " << average_rooms_per_event << endl;
  os << "Number of anyroom events: " << (free_events * 100.0)/ in.events << "%" << endl;

  return os;
}

TT_Output::TT_Output(const TT_Input& pin)
  : in(pin), timeslot(in.Events()), room(in.Events())
{}

TT_Output::TT_Output(const TT_Output& out)
  :  in(out.in), timeslot(out.timeslot), room(out.room) {}

TT_Output& TT_Output::operator=(const TT_Output& out)
{
  timeslot = out.timeslot;
  room = out.room;
  return *this;
}

ostream& operator<<(ostream& os, const TT_Output& out)
{
  for (unsigned e = 0; e < out.in.Events(); e++)
    if (out.timeslot[e] < out.in.TIMESLOTS)
      os << out.timeslot[e] << ' ' << out.room[e] << endl;
    else
      os << "-1 -1" << endl;
  return os;
}

istream& operator>>(istream& is, TT_Output& out)
{
  int timeslot, room;
  for (unsigned e = 0; e < out.in.Events(); e++)
    {
      is >> timeslot >> room;
      if (timeslot >= 0)
	{
	  out.timeslot[e] = timeslot;
	  out.room[e] = room;
	}
      else // unassigned timeslot
	{
	  out.timeslot[e] = out.in.UNSCHEDULED;
	  out.room[e] = out.in.EventRoomList(e,0);
	}
    }
  return is;
}

bool TT_State::Check() const
{
  unsigned all_scheduled_events = 0, scheduled_in_room_events = 0, filled_roomslots = 0, unscheduled_events = 0, free_events = 0, ts, r, e;
  ostringstream oss;

  for (ts = 0; ts < in.TIMESLOTS; ts++)
    {
      all_scheduled_events += all_slot_events[ts].size();
      scheduled_in_room_events += roomed_slot_events[ts].size();
      for (r = 0; r < in.Rooms(); r++)
	if (slot_room_event[ts][r] != in.NO_EVENT)
	  filled_roomslots++;
    }
	
  for(e = 0; e < in.Events(); e++)
    if (timeslot[e] == in.UNSCHEDULED)
      unscheduled_events++;
	
  if (all_scheduled_events + unscheduled_events != in.Events())
    {
      oss << "In the state: number of scheduled events (" << all_scheduled_events << ") and number of unscheduled events (" <<  unscheduled_events
	  << ") do not sum up to the total number of events (" << in.Events() << ")";
      throw logic_error(oss.str());
    }
  
  for (e = 0; e < in.Events(); e++)
    if (in.AnyRoomEvent(e))
      free_events++;
  if (scheduled_in_room_events + unscheduled_events != in.Events() - free_events)
    {
      oss << "In the state: number of events scheduled in rooms (" << scheduled_in_room_events << ") number of unscheduled events (" <<  unscheduled_events
	  << ") do not sum up to the total number of room-requesting events (" << (in.Events() - free_events) << ")";
      throw logic_error(oss.str());
    }

  if (filled_roomslots != scheduled_in_room_events)
 {
      oss << "In the state: number of events scheduled in rooms (" << scheduled_in_room_events << ") and the number of used roomslots (" <<  filled_roomslots
	  << ") do not match";
      throw logic_error(oss.str());
    }
  for (e = 0; e < in.Events(); e++)
    {
      EventList::const_iterator a;
      ts = timeslot[e];
      if (ts != in.UNSCHEDULED)
      for (a = all_slot_events[ts].begin(); a != all_slot_events[ts].end(); a++)
	if (*a == e)
	  break;
      if (a == all_slot_events[ts].end())
	throw logic_error("In the state, the event e is incorrectly scheduled (all_slot_events is not consistent)");
    }
  
  for (e = 0; e < in.Events(); e++)
    {
      ts = timeslot[e];
      r = room[e];
      if (ts != in.UNSCHEDULED && r < in.Rooms())
	{
	  if (slot_room_event[ts][r] != int(e))
	    cerr << ts << ' ' << r << ' ' << e << endl;
	  if (slot_room_event[ts][r] != int(e))
	    throw logic_error("In the state, the event e is incorrectly scheduled (slot_room_event is not consistent)");
	  if (!in.EventRoomCompatibility(e,r))
	    {
	      oss << "In the state, the event " << e << " is scheduled in an incompatible room " << r;
	      throw logic_error(oss.str());
	    }
	}
    }
  
  return true;
}

ostream& operator<<(ostream& os, const TT_State& st)
{
  unsigned e, r, ts, s, d;
  os << "State:" << endl;
  for (e = 0; e < st.in.Events(); e++)
    os << st.timeslot[e] << ' ' << st.room[e] << endl;
  os << endl;

  os << "Slot: list of events in the slot" << endl;
  for (ts = 0; ts < st.in.TIMESLOTS; ts++)
    {
      if (st.all_slot_events[ts].size() > 0)
	{
	  os << ts << ": ";
	  for (TT_State::EventList::const_iterator a = st.all_slot_events[ts].begin(); a != st.all_slot_events[ts].end(); a++)
	    os << *a << ' ';
	  os << "| ";
	  for (TT_State::EventList::const_iterator a = st.roomed_slot_events[ts].begin(); a != st.roomed_slot_events[ts].end(); a++)
	    os << *a << ' ';
	  os << endl;
	}
    }
  os << endl;

  os << "Room/Slot: events in the room/slot" << endl;
  for (r = 0; r < st.in.Rooms(); r++)
    {
      os << r << ": ";
      for (ts = 0; ts < st.in.TIMESLOTS; ts++)
	{
	  if (st.slot_room_event[ts][r] != st.in.NO_EVENT)
	    os << ts << "->" << st.slot_room_event[ts][r] << ", ";
	}
      os << endl;
    }
  os << endl;

  os << "Student daily events" << endl;
  for (s = 0; s < st.in.Students(); s++)
    {
      for (d = 0; d < st.in.DAYS; d++)
	os << st.student_daily_events[s][d] << ' ';
      os << endl;
    }
  os << endl;

  os << "Student slot-triplet events" << endl;
  for (s = 0; s < st.in.Students(); s++)
    {
      for (ts = 0; ts < st.in.TIMESLOTS; ts++)
	{
	  os << st.student_triplet_events[s][ts] << ' ';
	  if (ts % st.in.DAY_SLOTS == st.in.DAY_SLOTS - 1)
	    os << "  ";
	}
      os << endl;
    }
  os << endl;

  return os;
}

TT_State::TT_State(const TT_Input& pin)
  : in(pin), timeslot(in.Events()), room(in.Events()),
    all_slot_events(in.TIMESLOTS), roomed_slot_events(in.TIMESLOTS), 
    slot_room_event(in.TIMESLOTS, vector<int> (in.Rooms() + 1, in.NO_EVENT)),
    student_daily_events(in.Students(), vector<unsigned>(in.DAYS)),
    student_triplet_events(in.Students(), vector<unsigned>(in.TIMESLOTS))
{}

TT_State::TT_State(const TT_State& s) 
  :   in(s.in), timeslot(s.timeslot), room(s.room),
      all_slot_events(s.all_slot_events), roomed_slot_events(s.roomed_slot_events), 
      slot_room_event(s.slot_room_event),
      student_daily_events(s.student_daily_events),
      student_triplet_events(s.student_triplet_events)
{}

TT_State& TT_State::operator=(const TT_State& s)
{
  timeslot = s.timeslot;
  room = s.room;
  all_slot_events = s.all_slot_events; 
  slot_room_event = s.slot_room_event;
  student_daily_events = s.student_daily_events;
  student_triplet_events = s.student_triplet_events;  
  roomed_slot_events = s.roomed_slot_events; 
  return *this;
} 

bool operator==(const TT_State& st1, const TT_State& st2)
{
  return st1.timeslot == st2.timeslot && st1.room == st2.room;
}

void TT_State::ClearRedundantData()
{
  unsigned ts, s, d;
  // clear old data
  for (ts = 0; ts < in.TIMESLOTS; ts++)
    {
      all_slot_events[ts].clear();
      roomed_slot_events[ts].clear();
    }

  for (s = 0; s < in.Students(); s++)
    {
      for (d = 0; d < in.DAYS; d++)
	student_daily_events[s][d] = 0;
      for (ts = 0; ts < in.TIMESLOTS; ts++)
	student_triplet_events[s][ts] = 0;
    }

  for (ts = 0; ts < in.TIMESLOTS; ts++)
    {
      for (unsigned r = 0; r < in.Rooms(); r++)
	slot_room_event[ts][r] = in.NO_EVENT;
    }

}

void TT_State::UpdateAdditionalRedundantData()
{
  unsigned ts, e, a, day, slot_position;

  for (e = 0; e < in.Events(); e++)
    {
      ts = timeslot[e];
      if (ts != in.UNSCHEDULED) // the event is scheduled
	{
	  day = ts/in.DAY_SLOTS;
	  slot_position = ts % in.DAY_SLOTS;
	  for (a = 0; a < in.EventStudentNumber(e); a++)
	    {	
	      student_daily_events[in.EventStudentList(e,a)][day]++;
	      if (ts > 0)
		student_triplet_events[in.EventStudentList(e,a)][ts-1]++;	
	      student_triplet_events[in.EventStudentList(e,a)][ts]++;	
	      if (ts < in.TIMESLOTS-1)
		student_triplet_events[in.EventStudentList(e,a)][ts+1]++;
	    }
	}
    }
}

void TT_State::UpdateTimeSlotRedundantData()
{
  unsigned ts, e, a, day, slot_position;

  for (e = 0; e < in.Events(); e++)
    {
      ts = timeslot[e];
      if (ts != in.UNSCHEDULED) // the event is scheduled
	{
	  day = ts/in.DAY_SLOTS;
	  slot_position = ts % in.DAY_SLOTS;
	  all_slot_events[ts].push_back(e);
	  if (!in.AnyRoomEvent(e))
	    roomed_slot_events[ts].push_back(e);
	  for (a = 0; a < in.EventStudentNumber(e); a++)
	    {	
	      student_daily_events[in.EventStudentList(e,a)][day]++;
	      if (ts > 0)
		student_triplet_events[in.EventStudentList(e,a)][ts-1]++;	
	      student_triplet_events[in.EventStudentList(e,a)][ts]++;	
	      if (ts < in.TIMESLOTS-1)
		student_triplet_events[in.EventStudentList(e,a)][ts+1]++;
	    }
	}
    }
}

void TT_State::UpdateRoomRedundantData()
{
  unsigned ts, e, r;

  for (e = 0; e < in.Events(); e++)
    {
      ts = timeslot[e];
      r = room[e];
      if (ts != in.UNSCHEDULED && r < in.Rooms()) // the event is scheduled and uses a specified room
	slot_room_event[ts][r] = e;
    }
}

void TT_State::UpdateRedundantData()
{
  ClearRedundantData();
  UpdateTimeSlotRedundantData();
  UpdateRoomRedundantData();
}


void TT_State::InsertSlotEvent(unsigned ts, unsigned e)
{
  if (!in.AnyRoomEvent(e))
    roomed_slot_events[ts].push_back(e);
  all_slot_events[ts].push_back(e);
}

void TT_State::RemoveSlotEvent(unsigned ts, unsigned e)
{
  EventList::iterator i;

  i = all_slot_events[ts].begin();
  while (true)
    {
      if (*i == e)
	{
	  all_slot_events[ts].erase(i);
	  break;
	}
      i++;
    }
  if (!in.AnyRoomEvent(e))
    {
      i = roomed_slot_events[ts].begin();
      while (true)
	{
	  if (*i == e)
	    {
	      roomed_slot_events[ts].erase(i);
	      break;
	    }
	  i++;
	}
    }
}

void TT_State::ReplaceSlotEvent(unsigned ts, unsigned e1, unsigned e2)
{
  // Da rivedere!!!
  RemoveSlotEvent(ts,e1);
  InsertSlotEvent(ts,e2);
}

// void TT_State::InsertSlotRoomEvent(unsigned ts, unsigned r, unsigned e)
// { // insert event in such a way that events are ordered by students (to compute cost of clashes)
//   vector<unsigned>::iterator i = slot_room_events[ts][r].begin();

//   if (r >= in.Rooms()) return;
//   while (i != slot_room_events[ts][r].end() && 
// 	 in.EventStudentNumber(e) <= in.EventStudentNumber(*i))
//     i++;
//   slot_room_events[ts][r].insert(i,e);
// }
      
// void TT_State::RemoveSlotRoomEvent(unsigned ts, unsigned r, unsigned e)
// { 
//   unsigned a = 0;
//   if (r == in.Rooms()) return;

//   while (slot_room_events[ts][r][a] != e)
//     a++;
//   slot_room_events[ts][r].erase(slot_room_events[ts][r].begin() + a);
// }
      
// unsigned TT_State::InsertSlotRoomEventCost(unsigned ts, unsigned r, unsigned e) const
// {// compute di RoomClash cost of inserting event e in room r in timeslot ts (assuming at least one event in)
//     if (in.EventStudentNumber(e) > in.EventStudentNumber(slot_room_events[ts][r][0]))
//       return in.EventStudentNumber(slot_room_events[ts][r][0]);
//     else
//       return in.EventStudentNumber(e);
// }

// unsigned TT_State::RemoveSlotRoomEventCost(unsigned ts, unsigned r, unsigned e) const
// {// compute di RoomClash cost of removing event e in room r in timeslot ts (assuming at least one event in)
//     if (e == slot_room_events[ts][r][0])
//       return in.EventStudentNumber(slot_room_events[ts][r][1]);
//     else
//       return in.EventStudentNumber(e);
// }

// int TT_State::ReplaceSlotRoomEventCost(unsigned ts, unsigned r, unsigned e1, unsigned e2) const
// {
//   if (e1 == slot_room_events[ts][r][0])
//     if (in.EventStudentNumber(e2) >= in.EventStudentNumber(slot_room_events[ts][r][1]))
//       // e2 replaces e1 has first element of the list: no cost
//       return 0;
//     else
//       // cost is reduced by the difference below
//       return in.EventStudentNumber(e2) - in.EventStudentNumber(slot_room_events[ts][r][1]);
//  else
//    if (in.EventStudentNumber(e2) >= in.EventStudentNumber(slot_room_events[ts][r][0]))
//      return in.EventStudentNumber(slot_room_events[ts][r][0]) - in.EventStudentNumber(e1);
//    else     
//      return in.EventStudentNumber(e2) - in.EventStudentNumber(e1);
// }

void TT_State::IncreaseStudentDailyEvents(unsigned e, unsigned d)
{
  unsigned a, s;

  for (a = 0; a < in.EventStudentNumber(e); a++)
    {
      s = in.EventStudentList(e,a);
      student_daily_events[s][d]++;
    }
}
 
void TT_State::DecreaseStudentDailyEvents(unsigned e, unsigned d)
{
  unsigned a, s;

  for (a = 0; a < in.EventStudentNumber(e); a++)
    {
      s = in.EventStudentList(e,a);
      student_daily_events[s][d]--;
    }
}

void TT_State::TransferStudentDailyEvents(unsigned e, unsigned d1, unsigned d2)
{
  unsigned a, s;

  for (a = 0; a < in.EventStudentNumber(e); a++)
    {
      s = in.EventStudentList(e,a);
      student_daily_events[s][d1]--;
      student_daily_events[s][d2]++;
    }
}


void TT_State::IncreaseStudentTripletEvents(unsigned e, unsigned ts)
{
  unsigned s, a, position = ts % in.DAY_SLOTS;
  for (a = 0; a < in.EventStudentNumber(e); a++)
    {
      s = in.EventStudentList(e,a);
      if (position > 1)
	student_triplet_events[s][ts-1]++;
      if (position > 0 && position < in.DAY_SLOTS-1)
	student_triplet_events[s][ts]++;
      if (position < in.DAY_SLOTS - 2)
	student_triplet_events[s][ts+1]++;
    } 
}

void TT_State::DecreaseStudentTripletEvents(unsigned e, unsigned ts)
{
  unsigned s, a, position = ts % in.DAY_SLOTS;
  for (a = 0; a < in.EventStudentNumber(e); a++)
    {
      s = in.EventStudentList(e,a);
      if (position > 1)
	student_triplet_events[s][ts-1]--;
      if (position > 0 && position < in.DAY_SLOTS-1)
	student_triplet_events[s][ts]--;
      if (position < in.DAY_SLOTS - 2)
	student_triplet_events[s][ts+1]--;
    }

}

int TT_State::ReplaceEventTripletCost(unsigned e1, unsigned e2, unsigned ts) const
{ 
  unsigned a, position = ts % in.DAY_SLOTS, s;
  int cost = 0;

  for (a = 0; a < in.EventStudentNumber(e1); a++)
    {
      s = in.EventStudentList(e1,a);
      if (!in.StudentEventEnrolment(s,e2))
	{
	  if (position > 1 
	      && student_triplet_events[s][ts-1] >= 3)
	    cost--;
	  if (position > 0 && position < in.DAY_SLOTS - 1 
	      && student_triplet_events[s][ts] >= 3)
	    cost--;
	  if (position < in.DAY_SLOTS - 2 
	      && student_triplet_events[s][ts+1] >= 3)
	    cost--;
	}
    }
  for (a = 0; a < in.EventStudentNumber(e2); a++)
    {
      s = in.EventStudentList(e2,a);
      if (!in.StudentEventEnrolment(s,e1))
	{
	  if (position > 1 
	      && student_triplet_events[s][ts-1] >= 2)
	    cost++;
	  if (position > 0 && position < in.DAY_SLOTS - 1 
	      && student_triplet_events[s][ts] >= 2)
	    cost++;
	  if (position < in.DAY_SLOTS - 2 
	      && student_triplet_events[s][ts+1] >= 2)
	    cost++;
	}
    }
  return cost;
}

unsigned TT_State::RemoveEventTripletCost(unsigned e, unsigned ts) const
{
  unsigned a, position = ts % in.DAY_SLOTS, s, cost = 0;
  for (a = 0; a < in.EventStudentNumber(e); a++)
    {
      s = in.EventStudentList(e,a);
      if (position > 1 
	  && student_triplet_events[s][ts-1] >= 3)
	cost++;
      if (position > 0 && position < in.DAY_SLOTS - 1 
	  && student_triplet_events[s][ts] >= 3)
	cost++;
      if (position < in.DAY_SLOTS - 2 
	  && student_triplet_events[s][ts+1] >= 3)
	cost++;
      }
   return cost;
 
}

unsigned TT_State::InsertEventTripletCost(unsigned e, unsigned ts) const
{
  unsigned a, position = ts % in.DAY_SLOTS, s, cost = 0;
  for (a = 0; a < in.EventStudentNumber(e); a++)
    {
      s = in.EventStudentList(e,a);
      if (position > 1 
	  && student_triplet_events[s][ts-1] >= 2)
	cost++;
      if (position > 0 && position < in.DAY_SLOTS - 1 
	  && student_triplet_events[s][ts] >= 2)
	cost++;
      if (position < in.DAY_SLOTS - 2 
	  && student_triplet_events[s][ts+1] >= 2)
	cost++;
    }
  return cost;
}
 
unsigned TT_State::StudentClashCost(unsigned e, unsigned ts) const
{
  unsigned cost = 0;

  for (TT_State::EventList::const_iterator e1 = all_slot_events[ts].begin(); e1 != all_slot_events[ts].end(); e1++)  
    {
      if (in.EventConflict(e,*e1) > 0 && e != *e1)
	cost += min(in.EventStudentNumber(e), in.EventStudentNumber(*e1));
    }
  return cost;
}

unsigned TT_State::StudentClashCost2(unsigned e, unsigned ts) const
{
  unsigned cost = 0;

  for (TT_State::EventList::const_iterator e1 = all_slot_events[ts].begin(); e1 != all_slot_events[ts].end(); e1++)  
    {
      if (in.EventConflict(e,*e1) > 0 && e != *e1)
	cost++;
    }
  return cost;
}

unsigned TT_State::StudentClashCost(unsigned e, unsigned ts, unsigned e_excluded) const
{ // same as StudentClashCost(e,ts) but without counting e_excluded
  unsigned cost = 0;

  for (TT_State::EventList::const_iterator e1 = all_slot_events[ts].begin(); e1 != all_slot_events[ts].end(); e1++)  
    {
      if (in.EventConflict(e,*e1) > 0 && e != *e1 && e_excluded != *e1)
	cost += min(in.EventStudentNumber(e), in.EventStudentNumber(*e1));
    }
  return cost;
}

unsigned TT_State::EventPrecedenceCost(unsigned e, unsigned ts) const
{
  unsigned a, p, e1, cost = 0;
  for (a = 0; a < in.EventBeforeNumber(e); a++)
    {
      e1 = in.EventBeforeList(e,a);
      p = timeslot[e1]; // period of the event that must be before e
      if (ts <= p && p < in.TIMESLOTS)
	{
	  cost += min(in.EventStudentNumber(e), in.EventStudentNumber(e1));
	}
    }
  for (a = 0; a < in.EventAfterNumber(e); a++)
    {
      e1 = in.EventAfterList(e,a);
      p = timeslot[e1]; // period of the event that must be after e
      if (ts >= p)
	{
	  cost += min(in.EventStudentNumber(e), in.EventStudentNumber(e1));
	}
     }

  return cost;
}

unsigned TT_State::FindFreeRoom(unsigned e, unsigned ts) const
{ // return in.DUMMY_ROOM if no room available

  unsigned index = 0, room;

  if (in.AnyRoomEvent(e) || ts == in.UNSCHEDULED)
    return in.ANY_ROOM;
  while (index < in.EventRoomNumber(e))
    {
      room = in.EventRoomList(e,index);
      if (SlotRoomFree(ts,room))
	return index;
      else
	index++;
    }
  return in.DUMMY_ROOM;
}

unsigned TT_State::FindFreeRoom(unsigned e, unsigned ts, unsigned r) const
{ //   consider r as free, return the room rather than the index

  unsigned index = 0, room;

  if (in.AnyRoomEvent(e) || ts == in.UNSCHEDULED)
    return in.ANY_ROOM;
  while (index < in.EventRoomNumber(e))
    {
      room = in.EventRoomList(e,index);
      if (SlotRoomFree(ts,room) || room == r)
	return room; // index;
      else
	index++;
    }
  return in.DUMMY_ROOM;
}

int TT_State::OneClassPerDayTransferCost(unsigned e, unsigned d1, unsigned d2, unsigned e_r) const
// OneClassPerDay cost of trasferring event e from day d1 to day d2 (with e_r that goes away)
{
  unsigned a, s;  
  int cost = 0;
  
  for (a = 0; a < in.EventStudentNumber(e); a++)
    {
      s = in.EventStudentList(e,a);
      if (!in.StudentEventEnrolment(s,e_r))
	{
	  if (student_daily_events[s][d2] == 1)
	    cost--;
	  if (student_daily_events[s][d2] == 0)
	    cost++;
	  if (student_daily_events[s][d1] == 2)
	    cost++;
	  if (student_daily_events[s][d1] == 1)
	    cost--;
	}
    }
  return cost;
}

int TT_State::OneClassPerDayInsertCost(unsigned e, unsigned d, unsigned e_r) const
// OneClassPerDay cost of inserting event e in day d (with e_r that goes away)
{
  unsigned a, s;  
  int cost = 0;
  
  for (a = 0; a < in.EventStudentNumber(e); a++)
    {
      s = in.EventStudentList(e,a);
      if (!in.StudentEventEnrolment(s,e_r))
	{
	  if (student_daily_events[s][d] == 1)
	    cost--;
	  if (student_daily_events[s][d] == 0)
	    cost++;
	}
    }
  return cost;
}


int TT_State::OneClassPerDayRemoveCost(unsigned e, unsigned d, unsigned e_r) const
// OneClassPerDay cost of trasferring event e from day d1 to day d2 (with e_r that goes away)
{
  unsigned a, s;  
  int cost = 0;
  
  for (a = 0; a < in.EventStudentNumber(e); a++)
    {
      s = in.EventStudentList(e,a);
      if (!in.StudentEventEnrolment(s,e_r))
	{
	  if (student_daily_events[s][d] == 2)
	    cost++;
	  if (student_daily_events[s][d] == 1)
	    cost--;
	}
    }
  return cost;
}

unsigned TT_State::TripletCost(unsigned ts, unsigned e1, unsigned e2) const
{ // cost of Triplets in timeslot ts only from students in events e1 and e2
  unsigned a, s, p = ts % in.DAY_SLOTS;  
  unsigned cost = 0;

  for (a = 0; a < in.EventStudentNumber(e1); a++)
    {
      s = in.EventStudentList(e1,a);
      if (p > 0 
	  && student_triplet_events[s][ts-1] >= 3)
	cost += student_triplet_events[s][ts-1] - 2;	      
      if (student_triplet_events[s][ts] >= 3)
	cost += student_triplet_events[s][ts] - 2;	      
       if (p < in.DAY_SLOTS - 1 
	  && student_triplet_events[s][ts+1] >= 3)
	cost += student_triplet_events[s][ts+1] - 2;	      
    }
  for (a = 0; a < in.EventStudentNumber(e2); a++)
    {
      s = in.EventStudentList(e2,a);
      if (!in.StudentEventEnrolment(s,e1))
	{
	  if (p > 0 
	      && student_triplet_events[s][ts-1] >= 3)
	    cost += student_triplet_events[s][ts-1] - 2;	      
	  if (student_triplet_events[s][ts] >= 3)
	    cost += student_triplet_events[s][ts] - 2;	      
	  if (p < in.DAY_SLOTS - 1 
	      && student_triplet_events[s][ts+1] >= 3)
	    cost += student_triplet_events[s][ts+1] - 2;	      
	}
    }
  return cost;
}

unsigned TT_State::TripletCost(unsigned e, unsigned ts) const
{
  unsigned a, s, p = ts % in.DAY_SLOTS;  
  unsigned cost = 0;

  for (a = 0; a < in.EventStudentNumber(e); a++)
    {
      s = in.EventStudentList(e,a);
      if (p > 1 
	  && student_triplet_events[s][ts-1] >= 3)
	cost += student_triplet_events[s][ts-1] - 2;	      
      if (p > 0 && p < in.DAY_SLOTS - 1 
	  && student_triplet_events[s][ts] >= 3)
	cost += student_triplet_events[s][ts] - 2;	      
       if (p < in.DAY_SLOTS - 2 
	  && student_triplet_events[s][ts+1] >= 3)
	cost += student_triplet_events[s][ts+1] - 2;	      
    }
  return cost;
}

ostream& operator<<(ostream& os, const MoveEvent& mv)
{
  return os << mv.event << ":(" << mv.old_slot << ',' << mv.old_room 
     << ")-->(" << mv.new_slot << ',' << mv.new_room << ")";
}

istream& operator>>(istream& is, MoveEvent& mv)
{
  char ch;
  return is >> mv.event >> ch >> ch >> mv.old_slot >> ch >> mv.old_room 
	    >> ch >> ch >> ch >> ch >> ch
	    >> mv.new_slot >> ch >> mv.new_room >> ch;
}

bool operator<(const MoveEvent& mv1, const MoveEvent& mv2)
{
  return (mv1.event < mv2.event)
    || (mv1.event == mv2.event && mv1.new_slot < mv2.new_slot)
    || (mv1.event == mv2.event && mv1.new_slot == mv2.new_slot && mv1.new_room < mv2.new_room);
}

ostream& operator<<(ostream& os, const SwapTimeAndRoom& mv)
{
  return os << mv.event1 << " (" << mv.slot1 << ',' << mv.room1 << ") <-> " 
	    << mv.event2 << " (" << mv.slot2 << ',' << mv.room2 << ")";
}

istream& operator>>(istream& is, SwapTimeAndRoom& mv)
{
  char ch;
  return is >> mv.event1 >> ch >> mv.slot1 >> ch >> mv.room1 >> ch >> ch >> ch >> ch
	    >> mv.event2 >> ch >> mv.slot2 >> ch >> mv.room2 >> ch;
}

bool operator<(const SwapTimeAndRoom& mv1, const SwapTimeAndRoom& mv2)
{
  return (mv1.event1 < mv2.event1)
    || (mv1.event1 == mv2.event1 && mv1.event2 < mv2.event2);
}

ostream& operator<<(ostream& os, const SwapTime& mv)
{
  return os << mv.event1 << " (" << mv.slot1 << ',' << mv.old_room1 << "->" << mv.new_room1 << ") <-> " 
	    << mv.event2 << " (" << mv.slot2 << ',' << mv.old_room2 << "->" << mv.new_room2 << ")";
}

istream& operator>>(istream& is, SwapTime& mv)
{
  char ch;
  return is >> mv.event1 >> ch >> mv.slot1 >> ch >> mv.old_room1 >> ch >> ch >>  mv.new_room1 >> ch >> ch >> ch >> ch
	    >> mv.event2 >> ch >> mv.slot2 >> ch >> mv.old_room2 >> ch >> ch >>  mv.new_room2 >> ch;
}

bool operator<(const SwapTime& mv1, const SwapTime& mv2)
{
  return (mv1.event1 < mv2.event1)
    || (mv1.event1 == mv2.event1 && mv1.event2 < mv2.event2);
}

